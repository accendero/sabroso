import Constants from '../constants';
import {request} from './requests';

export function requestProjects() {
  return {
    type: Constants.REQUEST_PROJECTS
  }
}

export function receiveProjects(json) {
  return {
    type: Constants.RECEIVE_PROJECTS,
    projects: json
  }
}

export function sharingProject() {
  return {
    type: Constants.SHARING_PROJECT
  }
}

export function completeProjectShare(json) {
  return {
    type: Constants.COMPLETE_PROJECT_SHARE,
    message: json
  }
}

export function errorReceiveProjects(error) {
  return {
    type: Constants.ERROR_RECEIVE_MENU
  }
}

export function fetchProjects(authHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    dispatch(requestProjects());
    return request(
      urls.base+urls.get_projects.url+urls.suffix,
      urls.get_projects.method,
      authHeader,
      null
    )
    .catch(error => (dispatch(errorReceiveProjects(error))))
    .then(json => (dispatch(receiveProjects(json.results))));
  }
}

export function addProject(project, postHeader, authHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    return request(
      urls.base+urls.add_project.url,
      urls.add_project.method,
      postHeader,
      JSON.stringify(project)
    )
    .then(() => (dispatch(fetchProjects(authHeader))));
  }
}

export function removeProject(project_id, authHeader, postHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    return request(
      urls.base+urls.remove_project.url+project_id+'/',
      urls.remove_project.method,
      postHeader,
      null,
      true
    )
    .then(() => (dispatch(fetchProjects(authHeader))));
  }
}

export function shareProject(project, user, authHeader, postHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    dispatch(sharingProject());
    return request(
      urls.base+urls.share_project.url,
      urls.share_project.method,
      postHeader,
      JSON.stringify({project: project, user: user})
    )
    .then(json => (dispatch(completeProjectShare(json))))
  }
}

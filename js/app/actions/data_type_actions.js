import Constants from '../constants';
import {request} from './requests';

export function requestDataTypes() {
  return {
    type: Constants.REQUEST_DATA_TYPES
  }
}

export function receiveDataTypes(json) {
  return {
    type: Constants.RECEIVE_DATA_TYPES,
    dataTypes: json
  }
}

export function fetchDataTypes(authHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    dispatch(requestDataTypes());
    return request(
      urls.base+urls.get_data_types.url+urls.suffix,
      urls.get_data_types.method,
      authHeader,
      null
    )
    .catch(error => console.log(error))
    .then(json => (dispatch(receiveDataTypes(json.results))));
  }
}

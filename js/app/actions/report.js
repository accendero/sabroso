import Constants from '../constants';
import {request} from './requests';
import {plotRedraw} from "./user_actions";
import {fetchSavesForProject} from "./save_actions";

export function receiveReport(report) {
    return {
        type: Constants.REPORT_RECEIVE,
        report
    }
}

export function receiveReports(reports) {
    return {
        type: Constants.RECEIVE_REPORTS,
        reports
    }
}

export function requestReports() {
    return {
        type: Constants.REQUEST_REPORTS,
    }
}

export function requestReportItems() {
    return {
        type: Constants.REPORT_REQUEST,
    }
}

export function receiveReportItems(report) {
    return {
        type: Constants.REPORT_RECEIVE,
        report,
    }
}

export function errorReportItems(error) {
    return {
        type: Constants.REPORT_ERROR,
        error,
    }
}

export function setReportSaveSelection(save) {
    return {
        type: Constants.REPORT_SET_SAVE_SELECTION,
        addSave: save,
    }
}

export function setReportText(text) {
    return {
        type: Constants.REPORT_SET_TEXT,
        addText: text,
    }
}

function urlforthis(project_id) {
    const urls = Constants.api_urls;
    return urls.base + urls.get_projects.url + project_id + '/' +
        urls.get_project_reports.url + urls.suffix;
}

export function fetchReports(project_id, authHeader) {
    return dispatch => {
        const urls = Constants.api_urls;

        dispatch(requestReports());
        return request(
            urlforthis(project_id),
            urls.get_project_reports.method,
            authHeader,
            null
        )
            .catch(
                error => (dispatch(errorReportItems(error)))
            )
            .then(json => (dispatch(receiveReports(json))));
    }
}

export function getReportItems(id, authHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        dispatch(requestReportItems());
        return request(
            urls.base + urls.get_report_items.url + id + '/',
            urls.get_report_items.method,
            authHeader,
            null
        )
            .catch(error => (dispatch(errorReportItems(error))))
            .then(json => {
                let plotPrefix = 0;
                let reportItems = json.report_items.map(item => {
                    if (item.object_type === "AnalysisSave") {
                        let data = item.object_data,
                            config = data.analysis.analysis_type;
                        if (typeof config.controls !== 'object')
                            config.controls = JSON.parse(config.controls);
                        config.plotConfiguration = JSON.parse(config.plot_configuration);
                        config.transformationConfiguration = JSON.parse(config.transformation_configuration);
                        data.configuration = JSON.parse(data.configuration);
                    }
                    item.plotPrefix = plotPrefix;
                    plotPrefix += 100;
                    return item;
                });
                json.report_items = reportItems;
                dispatch(receiveReportItems(json));
                dispatch(plotRedraw(true, {}));
                return json.project;
            })
            .then(project => dispatch(fetchSavesForProject(project.id, authHeader)))
    }
}

export function addReport(item, postHeader, authHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        return request(
            urls.base + urls.post_report.url,
            urls.post_report.method,
            postHeader,
            JSON.stringify(item),
        )
            .catch(error => console.log(error))
            .then(json => (dispatch(fetchReports(json.project, authHeader))));
    }
}

export function addReportItem(item, authHeader, postHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        return request(
            urls.base + urls.post_report_item.url,
            urls.post_report_item.method,
            postHeader,
            JSON.stringify(item),
        )
            .catch(error => console.log(error))
            .then(json => (dispatch(getReportItems(json.report, authHeader))));
    }
}

export function removeReportItem(itemId, reportId, authHeader, postHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        return request(
            urls.base + urls.remove_report_item.url + itemId + '/',
            urls.remove_report_item.method,
            postHeader,
            null
        )
            .catch(error => console.log(error))
            .then(() => dispatch(removeReportItemFromState(itemId)));
    }
}

export function removeReport(v_id, project, authHeader, postHeader) {
    return dispatch => {
        const urls = Constants.api_urls;
        return request(
            urls.base + urls.remove_report.url + v_id + '/',
            urls.remove_report.method,
            postHeader,
        )
            .catch(error => console.log(error))
            .then(() => dispatch(fetchReports(project, authHeader)));
    }
}

export function putReportItem(item, postHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        return request(
            urls.base + urls.put_report_item.url + item.id + '/',
            urls.put_report_item.method,
            postHeader,
            JSON.stringify(item),
        )
            .then(() => dispatch(moveReportItem(item)))
            .catch(error => console.log(error));
    }
}

export function removeReportItemFromState(id) {
    return {
        type: Constants.REMOVE_REPORT_ITEM,
        id: id,
    }
}

export function moveReportItem(item) {
    return {
        type: Constants.MOVE_REPORT_ITEM,
        item: item,
    }
}

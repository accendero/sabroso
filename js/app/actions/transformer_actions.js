import Immutable from 'immutable';
import Constants from '../constants';
import {request} from './requests';

export function requestData() {
    return {
        type: Constants.REQUEST_TRANSFORMED_DATA,
    }
}

export function receiveData(name, data, analysisType) {
    return {
        type: Constants.RECEIVE_TRANSFORMED_DATA,
        data: data[0],
        configuration: data[1],
        analysisType: analysisType,
        name: name,
    }
}

export function receiveAvailable(data) {
    return {
        type: Constants.RECEIVE_AVAILABLE,
        available: data
    }
}

export function errorReceiveData() {
    return {
        type: Constants.ERROR_RECEIVE_TRANSFORMED_DATA,
        data: [],
        meta: [],
    };
}

export function fetchAvailable(available, buckets, projectId, postHeader) {
    const urls = Constants.api_urls;

    return dispatch => {

        dispatch(requestData());

        return request(
            urls.base + urls.available.url,
            urls.available.method,
            postHeader,
            JSON.stringify({
                method: available,
                buckets: buckets,
                project: projectId
            })
        )
            .catch(error => (dispatch(errorReceiveData(error))))
            .then(data => (dispatch(receiveAvailable(data))))
    }
}

export function fetchData(
    name, pvtCnfg, buckets, filters, args, projectId, cols,
    analysisType, configFunc, configArgs, postHeader) {
    const urls = Constants.api_urls;

    return dispatch => {

        dispatch(requestData());

        let pivotConfiguration = Immutable.fromJS(pvtCnfg);
        pivotConfiguration = pivotConfiguration.set("project", projectId);
        pivotConfiguration = pivotConfiguration.set("buckets", buckets);
        pivotConfiguration = pivotConfiguration.set("columns", cols);
        pivotConfiguration = pivotConfiguration.set("filters", filters);
        pivotConfiguration = pivotConfiguration.set("kwargs", args);


        let pivotReq = request(
            urls.base + urls.pivot.url,
            urls.pivot.method,
            postHeader,
            JSON.stringify(pivotConfiguration.toJS())),
            configReq = request(
                urls.base + urls.config.url,
                urls.config.method,
                postHeader,
                JSON.stringify({
                    "analysis_type": analysisType,
                    "function": configFunc,
                    "args": configArgs,
                }));
        Promise.all([pivotReq, configReq])
            .catch(error => (dispatch(errorReceiveData(error))))
            .then(data => (dispatch(receiveData(name, data, analysisType))));
    }
}

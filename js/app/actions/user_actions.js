import Constants from '../constants';

export function appendCols(cols) {
  return {
    type: Constants.APPEND_COLUMN,
    cols: cols
  }
}

export function setControl(plotIndex, controlIndex, controlName, controlType, value) {
  return {
    type: Constants.SET_CONTROL,
    plotIndex,
    controlIndex,
    controlName,
    controlType,
    value,
  }
}

export function deletePlot(plot) {
  return {
    type: Constants.DELETE_PLOT,
    plot: plot
  }
}

export function deleteRow(plot, controlIndex, controlName) {
  return {
    type: Constants.DELETE_ROW,
    plot: plot,
    controlIndex: controlIndex,
    controlName: controlName
  }
}

export function plotRedraw(doRedraw, updateConfiguration) {
  return {
    type: Constants.PLOT_DRAWN,
    redraw: doRedraw,
    updateConfiguration: updateConfiguration
  }
}

export function mergePlots(source, target) {
  return {
    type: Constants.MERGE_PLOTS,
    source: source,
    target: target
  }
}

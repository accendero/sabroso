import Constants from '../constants';

export function logoutUser() {
  localStorage.removeItem('token');
  localStorage.removeItem('token-issued');
  localStorage.removeItem('username');
  return {
    type: Constants.LOGOUT_USER,
    token: null,
    user: null,
  };
}

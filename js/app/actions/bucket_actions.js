import Constants from '../constants';
import {request} from './requests';

export function requestBuckets() {
    return {
        type: Constants.REQUEST_BUCKETS
    }
}

export function receiveBuckets(json) {
    return {
        type: Constants.RECEIVE_BUCKETS,
        buckets: json
    }
}

export function completeBucketModification(json) {
  return {
    type: Constants.COMPLETE_BUCKET_MODIFICATION,
    message: json
  }
}

export function fetchBuckets(authHeader) {
    const urls = Constants.api_urls;

    return dispatch => {
        dispatch(requestBuckets());
        return request(
            urls.base+urls.get_buckets.url+urls.suffix,
            urls.get_buckets.method,
            authHeader,
            null
        )
        .catch(error => console.log(error))
        .then(json => (dispatch(receiveBuckets(json.results))));
    }
}

export function addProjectBucket(bid, pid, authHeader, postHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
      return request(
          urls.base+urls.add_project_bucket.url,
          urls.add_project_bucket.method,
          postHeader,
          JSON.stringify({
            'bucket': bid,
            'project': pid
          })
      )
      .then(json => (dispatch(completeBucketModification(json))))
      .then(() => (dispatch(fetchBuckets(authHeader))));
  }
}


export function addBucket(bucket, authHeader, postHeader) {
    const urls = Constants.api_urls;

    return dispatch => {
        return request(
            urls.base+urls.add_bucket.url,
            urls.add_bucket.method,
            postHeader,
            JSON.stringify(bucket)
        )
        .catch(error => console.log(error))
        .then(() => (dispatch(fetchBuckets(authHeader))));
    }
}

import fetch from 'isomorphic-fetch';

function makeHandler(noResponse) {
  return function handleResponse(response) {
    let json;
    if (noResponse) {
      json = ""
    } else {
      json = response.json();
    }
    if(!response.ok) {
      return json;
    }
    return json;
  }
}

export function request(url, method, requestHeaders, data, noResponse) {
  if(undefined !== requestHeaders &&
    requestHeaders.append &&
    typeof requestHeaders.append === 'function'){
    requestHeaders.append('Content-Type', 'application/json');
  }
  else {
    requestHeaders = Object.assign(
      {'Content-Type': 'application/json'}, requestHeaders
    );
  }

  let options = {
    method: method || 'GET',
    headers: requestHeaders,
    dataType: 'json',
  };

  if(data)
    options['body'] = data;

  return fetch(url, options).then(makeHandler(noResponse));
}

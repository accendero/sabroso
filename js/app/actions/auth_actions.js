import Constants from '../constants';
import {request} from './requests';

export function requestToken() {
  return { type: Constants.REQUEST_TOKEN };
}

export function receiveToken(token, username) {
  localStorage.setItem('token', token);
  localStorage.setItem('token-issued', new Date().getTime());
  localStorage.setItem('username', username);

  return {
    type: Constants.RECEIVE_TOKEN,
    token: token,
    user: username
  };
}

export function errorReceiveToken(token, username) {
  return {
    type: (undefined === token) ?
      Constants.ERROR_RECEIVE_TOKEN : Constants.RECEIVE_TOKEN,
    token: token,
    user: username,
  };
}

export function loadCookieToken() {
  const cookieToken = localStorage.getItem('token');
  const cookieUser = localStorage.getItem('username');

  return {
    type: Constants.RECEIVE_TOKEN,
    token: cookieToken,
    user: cookieUser,
  };
}

export function fetchToken(username, password) {
  const postHeader = new Headers();
  return dispatch => {
    dispatch(requestToken());

    const urls = Constants.api_urls;
    return request(
      urls.login_base+urls.get_token.url+urls.suffix,
      urls.get_token.method,
      postHeader,
      JSON.stringify({username: username, password: password})
    )
    .catch(error => console.log(error))
    .then(json => (dispatch(receiveToken(json.token, username))))
    .catch(error => (dispatch(errorReceiveToken(error.token, username))));
  }
}

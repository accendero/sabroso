import Constants from '../constants';

export function clearMessages() {
  return {
    type: Constants.CLEAR_MESSAGES
  }
}

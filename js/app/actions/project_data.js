import Constants from '../constants';
import {fetchAllAnalysisConfiguration} from './analysis_configuration_actions';
import {request} from './requests';


export function uploadingData() {
  return {
    type: Constants.UPLOADING_PROJECT_DATA
  }
}

export function uploadingSuccessful() {
  return {
    type: Constants.UPLOADING_PROJECT_DATA_SUCCESSFUL
  }
}

export function errorUploadingData(message) {
  return {
    type: Constants.ERROR_UPLOADING_PROJECT_DATA,
    message: message
  };
}

export function addData(data, project, dataType, authHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    dispatch(uploadingData());
    return request(
      urls.base+urls.add_project_data.url+dataType+'/'+project+'/',
      urls.add_project_data.method,
      authHeader,
      JSON.stringify(data)
    )
    .then(() => {
      dispatch(uploadingSuccessful());
      dispatch(fetchAllAnalysisConfiguration(authHeader))
    })
    .catch(error => (dispatch(errorUploadingData(error))));
  }
}

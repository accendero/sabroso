import Constants from '../constants';
import RestApi from '../utils/backend_rest';
import {request} from './requests';

export function requestSaves() {
    return {
        type: Constants.REQUEST_SAVES
    }
}

export function receiveSaves(project, analysis, saves) {
    return {
        type: Constants.RECEIVE_SAVES,
        analysis: analysis,
        project: project,
        saves: saves
    }
}

export function startShareAnalysis() {
    return {
        type: Constants.START_SHARE_ANALYSIS
    }
}

export function completeAnalysisShare(json) {
    return {
        type: Constants.COMPLETE_ANALYSIS_SHARE,
        message: json
    }
}

export function requestSave() {
    return {
        type: Constants.REQUEST_SAVE
    }
}

export function receiveSave(analysisSave) {
    return {
        type: Constants.RECEIVE_SAVE,
        analysisSave: analysisSave
    }
}

export function receiveQuery(configuration) {
    return {
        type: Constants.RECEIVE_QUERY,
        query: configuration.query
    }
}

export function wipeActiveSave() {
    return {
        type: Constants.RECEIVE_SAVE,
        analysisSave: null
    }
}

export function receiveProjectSaves(projectSaves) {
    return {
        type: Constants.RECEIVE_PROJECT_SAVES,
        saves: projectSaves,
    }
}

export function fetchSaves(analysisId, authHeader) {
    const urls = Constants.api_urls;

    return dispatch => {
        dispatch(requestSaves());
        let requestObj = {
            "authHeader": authHeader,
            "analysisId": analysisId
        }
        const fetchAnalysis = RestApi.fetchAnalysis(requestObj);
        const fetchSaves = RestApi.fetchAnalysisSaves(requestObj);

        return Promise.all([fetchAnalysis, fetchSaves]).then(function (data) {
            const analysis = data[0].analysis;
            const saves = data[1].analysisSaves.filter(
                save => (save.analysis == analysis.id)
            );
            requestObj.analysis = analysis;
            requestObj.saves = saves;
            requestObj.projectId = analysis.project;
            return Promise.resolve(requestObj);
        }).then(function (argObj) {
            return RestApi.fetchProject(requestObj);
        }).then(function (argObj) {
            dispatch(receiveSaves(argObj.project, argObj.analysis, argObj.saves));
        });
    }
}

export function fetchAnalysisSave(analysisSaveId, authHeader) {
    return dispatch => {
        let requestObj = {
            analysisSaveId: analysisSaveId,
            authHeader: authHeader,
        }
        dispatch(requestSave());

        return RestApi.fetchAnalysisSave(requestObj)
            .then(function (requestObj) {
                dispatch(receiveQuery(requestObj.analysisSave.configuration))
                dispatch(receiveSave(
                    requestObj.analysisSave
                ));
            });
    }
}

export function addAnalysisSave(save, postHeader, authHeader) {
    return dispatch => {
        let requestObj = {
            analysisSave: save,
            authHeader: authHeader,
            postHeader: postHeader
        }
        return RestApi.addAnalysisSave(requestObj).then(function () {
            return dispatch(fetchSaves(save.analysis, authHeader));
        });
    };
}

export function removeAnalysisSave(save_id, analysis, authHeader, postHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        return request(
            urls.base + urls.remove_analysis_save.url + save_id + '/',
            urls.remove_analysis_save.method,
            postHeader,
            null
        )
            .catch(error => console.log(error))
            .then(() => (dispatch(fetchSaves(analysis.id, authHeader))));
    }
}

export function shareAnalysisSave(analysis_save, user, authHeader, postHeader) {
    const urls = Constants.api_urls;

    return dispatch => {
        dispatch(startShareAnalysis());
        return request(
            urls.base + urls.share_analysis_save.url,
            urls.share_analysis_save.method,
            postHeader,
            JSON.stringify({analysis_save: analysis_save, user: user})
        )
            .catch(error => console.log(error))
            .then(json => (dispatch(completeAnalysisShare(json))));
    }
}

function urlforthis(project_id) {
    const urls = Constants.api_urls;
    return urls.base + urls.get_projects.url + project_id + '/' +
        urls.get_project_saves.url + urls.suffix;
}

export function fetchSavesForProject(project_id, authHeader) {
    const urls = Constants.api_urls;
    return dispatch => {
        return request(
            urlforthis(project_id),
            urls.get_project_saves.method,
            authHeader,
            null
        )
            .catch(
                error => console.log(error)
            )
            .then(json => (dispatch(receiveProjectSaves(json))));
    }
}

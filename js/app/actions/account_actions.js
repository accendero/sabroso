import Constants from '../constants';
import {request} from './requests';

export function requestUsers(json) {
  return {
    type: Constants.REQUEST_USERS,
    users: json
  }
}

export function receiveUsers(json) {
  return {
    type: Constants.RECEIVE_USERS,
    users: json
  }
}

export function passwordNoMatch() {
  return {
    type: Constants.PASSWORD_CHANGE_NOMATCH,
    message: 'Passwords do not match',
  };
}

export function resolveChangePassword(json) {
  if(json.success) {
    return {
      type: Constants.PASSWORD_CHANGED,
      message: json.success,
    };
  }
  else {
    const msg = (json.error) ? json.error : 'Could not change password';
    return {
      type: Constants.PASSWORD_CHANGE_ERROR,
      message: msg,
    };
  }
}

export function changePassword(authHeader, p1, p2) {
  const urls = Constants.api_urls;

  return dispatch => {
    if( p1 !== p2 )
      return dispatch(passwordNoMatch());

    dispatch({type: Constants.PASSWORD_CHANGE, message: 'Working...', });

    return request(
      urls.base+urls.account_change_password.url+urls.suffix,
      urls.account_change_password.method,
      authHeader,
      JSON.stringify({'password1': p1, 'password2': p2})
    )
    .catch(error => console.log(error))
    .then(json => (dispatch(resolveChangePassword(json))));
  }
}

export function fetchUsers(authHeader) {
  const urls = Constants.api_urls;

  return dispatch => {
    dispatch(requestUsers());
    return request(
      urls.base+urls.get_users.url+urls.suffix,
      urls.get_users.method,
      authHeader
    )
    .catch(error => console.log(error))
    .then(json => (dispatch(receiveUsers(json.results))));
  }
}

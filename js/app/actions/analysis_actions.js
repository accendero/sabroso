import {request} from './requests';
import Constants from '../constants';

export function requestAnalyses() {
  return {
    type: Constants.REQUEST_ANALYSES
  };
}

export function receiveAnalyses(project, analyses) {
  return {
    type: Constants.RECEIVE_ANALYSES,
    analyses: analyses,
    project: project
  };
}

export function startShareAnalysis() {
  return {
    type: Constants.START_SHARE_ANALYSIS
  };
}

export function completeAnalysisShare(json) {
  return {
    type: Constants.COMPLETE_ANALYSIS_SHARE,
    message: json
  };
}

export function errorReceiveAnalysis(error) {
  return {
    type: Constants.ERROR_RECEIVE_MENU
  };
}

export function clearCurrentAnalysis() {
  return {
    type: Constants.CLEAR_ANALYSIS
  };
}

export function fetchAnalyses(projectId, authHeader) {
  return function (dispatch) {
    const urls = Constants.api_urls;
    dispatch(requestAnalyses());

    const getProject = request(
      urls.base+urls.get_projects.url+projectId+'/'+urls.suffix,
      urls.get_projects.method,
      authHeader,
      null
    )
    .catch(
      error => (dispatch(errorReceiveAnalysis(error)))
    );

    const getAnalyses = request(
      urls.base+urls.get_analyses.url+urls.suffix,
      urls.get_analyses.method,
      authHeader,
      null
    )
    .catch(
      error => (dispatch(errorReceiveAnalysis(error)))
    );

    return Promise.all([getProject, getAnalyses])
    .then(data => {
      const project = data[0];
      const analyses = data[1].results.filter(function(analysis) {
        return analysis.project == project.id;
      })
      return dispatch(receiveAnalyses(project, analyses));
    });
  }
}

export function addAnalysis(analysis, postHeader, authHeader) {
  return dispatch => {
    const urls = Constants.api_urls;
    return request(
      urls.base+urls.add_analysis.url,
      urls.add_analysis.method,
      postHeader,
      JSON.stringify(analysis)
    )
    .catch(error => console.log(error))
    .then(() => (dispatch(fetchAnalyses(analysis.project, authHeader))));
  }
}

export function removeAnalysis(analysis_id, project, authHeader, postHeader) {
  return dispatch => {
    const urls = Constants.api_urls;
    return request(
      urls.base+urls.remove_analysis.url+analysis_id+'/',
      urls.remove_analysis.method,
      postHeader,
    )
    .catch(error => console.log(error))
    .then(() => dispatch(fetchAnalyses(project.id, authHeader)));
  }
}

export function shareAnalysis(analysis, user, authHeader, postHeader) {
  return dispatch => {
    const urls = Constants.api_urls;

    dispatch(startShareAnalysis());
    return request(
      urls.base+urls.share_analysis.url,
      urls.share_analysis.method,
      postHeader,
      JSON.stringify({analysis: analysis, user: user})
    )
    .catch(error => console.log(error))
    .then(data => dispatch(completeAnalysisShare(data)));
  }
}

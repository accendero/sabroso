import Constants from '../constants';
import {request} from './requests';

export function requestControl() {
  return {
    type: Constants.REQUEST_CONTROL
  };
}

export function receiveControl(dType, column, json, iMap) {
  return {
    type: Constants.RECEIVE_CONTROL,
    dataType: dType,
    column: column,
    values: json,
    itemMap: iMap
  };
}

export function fetchControl(dType, project, column, header, iMap) {
  const urls = Constants.api_urls;

  return dispatch => {
    dispatch(requestControl());
    return request(
      urls.base+urls.get_project_data.url+dType+'/'+project+'/'+column+'/',
      urls.get_projects.method,
      header
    )
    .catch(error => console.log(error))
    .then(json => (receiveControl(dType, column, json, iMap)));
  }
}

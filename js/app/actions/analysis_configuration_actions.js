import Constants from '../constants';
import {request} from './requests';

export function requestAnalysisConfiguration() {
  return {
    type: Constants.REQUEST_ANALYSIS_CONFIGURATION
  }
}

export function receiveAnalysisConfiguration(aType, pType, tConfig, uConfig) {
  return {
    type: Constants.RECEIVE_ANALYSIS_CONFIGURATION,
    analysisType: aType,
    pluginType: pType,
    transformationConfiguration: tConfig,
    updateConfiguration: uConfig
  }
}

export function requestAllAnalysisConfiguration() {
  return {
    type: Constants.REQUEST_ALL_ANALYSIS_CONFIGURATION
  }
}

export function receiveAllAnalysisConfiguration(analysisTypes) {
  return {
    type: Constants.RECEIVE_ALL_ANALYSIS_CONFIGURATION,
    analysisTypes: analysisTypes
  }
}

export function fetchAnalysisConfiguration(analysisId, authHeader) {
  return dispatch => {

    dispatch(requestAnalysisConfiguration());
    const urls = Constants.api_urls;

    return request(
      urls.base+urls.get_analyses.url+analysisId+'/'+urls.suffix,
      urls.get_analyses.method,
      authHeader
    )
    .then(analysis => {
      const getAnalysisType = request(
        urls.base+urls.get_analysis_types.url+analysis.analysis_type+'/'+urls.suffix,
        urls.get_analysis_types.method,
        authHeader
      )
      .catch(error => console.log(error))
      .then(analysisType =>
        (dispatch(receiveAnalysisConfiguration(
          analysisType.name,
          analysisType.plugin,
          JSON.parse(analysisType.transformation_configuration)),
          analysisType.update_configuration
        ))
      );
    });
  }
}

export function fetchAllAnalysisConfiguration(authHeader) {
  const urls = Constants.api_urls;
  return dispatch => {
    dispatch(requestAllAnalysisConfiguration());
    const getAnalysisType = request(
      urls.base+urls.get_analysis_types.url+urls.suffix,
      urls.get_analysis_types.method,
      authHeader
    )
    .catch(error => console.log(error))
    .then(analysisTypes => {
      analysisTypes = analysisTypes.results.map(at => {
        return {
          id: at.id,
          name: at.name,
          plugin: at.plugin,
          transformationConfiguration: JSON.parse(at.transformation_configuration),
          controls: JSON.parse(at.controls),
          data_types: at.data_types,
          active_projects: at.active_projects,
          available_method: at.available_method,
          updateConfiguration: at.update_configuration
        }
      });
      dispatch(receiveAllAnalysisConfiguration(analysisTypes));
    });
  }
}

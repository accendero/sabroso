var Immutable = require('immutable'),
    Constants = require('../constants.js');

const initialState = {
    cols: {},
    available: [],
    configuration: {},
    isCurrent: false,
    isFetching: false,
    isErrorFetching: false,
    plotWidgetName: '',
};

const update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_TRANSFORMED_DATA:
            return state.merge({
                isFetching: true,
                isCurrent: true,
                plotWidgetName: action.name,
            }).toJS();

        case Constants.RECEIVE_AVAILABLE:
            return state.merge({
                isFetching: false,
                available: action.available
            }).toJS();

        case Constants.RECEIVE_TRANSFORMED_DATA:
            var ret = state.merge({
                isFetching: false,
                isCurrent: true,
            });
            Object.keys(action.data).forEach(function(key) {
                ret = ret.setIn(["cols", action.name], action.data[key]);
            });
            ret = ret.setIn(["configuration", action.analysisType], action.configuration);
            return ret.toJS();

        case Constants.ERROR_RECEIVE_TRANSFORMED_DATA:
            return state.merge({
                isErrorFetching: true,
                isFetching: false,
                isCurrent: true
            }).toJS();

        case Constants.RECEIVE_SAVE:
            return state.merge({
                isCurrent: false
            }).toJS();

        case Constants.CLEAR_ANALYSIS:
            let ret = state.merge({
                isFetching: false,
                isErrorFetching: false,
                isCurrent: false,
                cols: {},
                available: []
            });
            return ret.toJS();
        default:
          return state.toJS();
    }
};

module.exports = update;

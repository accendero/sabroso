var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    header: null,
    postHeader: null,
    token: null,
    user: "unknown user",
    isFetching: false,
    isTokenError: 1,
};

var makeHeader = function makeHeader(token, ispost) {
    var authHeader = new Headers();
    authHeader.append("Authorization", "Token "+token);
    if (ispost) {
        authHeader.append("content-type", "application/json; charset=utf-8");
    }
    return authHeader;
}


var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_TOKEN:
            return state.merge({
                isFetching: true,
                isTokenError: false,
            }).toJS();
        case Constants.RECEIVE_TOKEN:
            var ret = state.merge({
                isFetching: false,
                isTokenError: false,
            });
            ret = ret.merge({
                "header": makeHeader(action.token),
                "postHeader": makeHeader(action.token, true),
                "token": action.token,
                "user": action.user,
            });
            return ret.toJS();
        case Constants.ERROR_RECEIVE_TOKEN:
            return state.merge({
                isFetching: false,
                isTokenError: true,
            }).toJS();
        case Constants.LOGOUT_USER:
            return state.merge({
              isFetching: false,
              isTokenError: false,
              token: null,
              user: null,
            }).toJS();

        default:
            return state.toJS();
    }
}

module.exports = update;

var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    users: [],
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }

    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_USERS:
            return state.merge({
                isFetching: true,
                users: [],
            }).toJS();
        case Constants.RECEIVE_USERS:
            return state.merge({
                isFetching: false,
                users: action.users,
            }).toJS();
        default:
            return state.toJS();
    }
}

module.exports = update;

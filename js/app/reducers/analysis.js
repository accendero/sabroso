var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    analysisType: null,
    isFetching: false,
    shareMessage: {},
    isSharing: false,
    isDoneSharing: false,
    plotCols: {},
    currentPlot: 0,
};

var update = function(state, action) {
    let defaultState = state;
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_ANALYSIS_CONFIGURATION:
            return state.merge({
                isFetching: true,
            }).toJS();

        case Constants.RECEIVE_ANALYSIS_CONFIGURATION:
            var ret = state.merge({
                isFetching: false,
                analysisType: action.analysisType,
            });
            return ret.toJS();

        case Constants.START_SHARE_ANALYSIS:
          return state.merge({
            isSharing: true,
            isDoneSharing: false,
            shareMessage: action.message
          }).toJS();

        case Constants.COMPLETE_ANALYSIS_SHARE:
          return state.merge({
            isSharing: false,
            isDoneSharing: true,
            shareMessage: action.message
          }).toJS();

        case Constants.CLEAR_MESSAGES:
            return state.merge({
              isSharing: false,
              isDoneSharing: false,
              shareMessage: {}
            }).toJS();
        case Constants.MERGE_PLOTS:

            let movingCols =  state.getIn(["plotCols", String(action.source)]),
                newCols = state.getIn(["plotCols", String(action.target)]);
            if (!movingCols | !newCols) {
              return state.toJS();
            }
            newCols = newCols.concat(movingCols);
            state = state.setIn(["plotCols", String(action.target)], newCols);
            state = state.deleteIn(["plotCols", String(action.source)]);
            return state.toJS();

        case Constants.DELETE_PLOT:
            state = state.deleteIn(["plotCols", String(action.plot)]);
            return state.toJS();

        case Constants.APPEND_COLUMN:
            state = state.setIn(["plotCols", state.get("currentPlot")], action.cols);
            return state.set("currentPlot", state.get("currentPlot") + 1).toJS();

        case Constants.CLEAR_ANALYSIS:
            return state.merge({
                "analysisType": null,
                "plotCols": {},
                "currentPlot": 0
            }).toJS();

        case Constants.RECEIVE_SAVE:
            if (action.analysisSave) {
                return Immutable.fromJS(action.analysisSave.configuration.analysis).toJS();
            } else {
                return Immutable.fromJS(initialState).toJS();
            }

        case Constants.REPORT_RECEIVE:
            let plotCols = {},
                colKeys;
            action.report.report_items.forEach(item => {
                if (item.object_type === "AnalysisSave") {
                    colKeys = item.object_data.configuration.analysis.plotCols;
                    Object.keys(colKeys).forEach(key => {
                        plotCols[Number(key)+item.plotPrefix] = colKeys[key];
                    });
                }
            });
            return state.set("plotCols", plotCols).toJS();

        default:
          if (defaultState) {
            return defaultState;
          } else {
            return state.toJS();
          }
    }
};

module.exports = update;

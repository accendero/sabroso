var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    items: [],
    messages: {},
    isFetching: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_BUCKETS:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_BUCKETS:
            return state.merge({
                isFetching: false,
                items: action.buckets
            }).toJS();
        case Constants.COMPLETE_BUCKET_MODIFICATION:
            return state.merge({
                messages: action.message
            }).toJS();
        case Constants.CLEAR_MESSAGES:
            return state.merge({
                shareMessage: {}
            }).toJS();
        default:
            return state.toJS();
    }
};

module.exports = update;

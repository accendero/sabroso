import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import MenuReducer from './menu.js';
import AnalysisReducer from './analysis.js';
import AuthenticationReducer from './auth.js';
import TransformedDataReducer from './transformed_data.js';
import UserStateReducer from './user.js';
import QueryReducer from './query.js';
import DataTypesReducer from './data_types.js';
import ProjectDataReducer from './project_data.js';
import AnalysisTypesReducer from './analysis_types.js';
import AccountReducer from './account.js';
import UsersReducer from './users.js';
import ReportReducer from './report';
import BucketsReducer from './buckets';

import {
  LOGOUT_USER,
} from '../constants';

const appReducer = combineReducers({
    menu: MenuReducer,
    analysis: AnalysisReducer,
    auth: AuthenticationReducer,
    analysisData: TransformedDataReducer,
    userState: UserStateReducer,
    query: QueryReducer,
    dataTypes: DataTypesReducer,
    projectData: ProjectDataReducer,
    analysisTypes: AnalysisTypesReducer,
    routing: routerReducer,
    account: AccountReducer,
    users: UsersReducer,
    report: ReportReducer,
    buckets: BucketsReducer,
});

// clear states on logout / auth failure
const rootReducer = (state, action) => {
  if( action.type === LOGOUT_USER )
  {
      state = undefined;
  }
  return appReducer(state, action);
}

export default rootReducer;

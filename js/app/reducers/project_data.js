var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    isSuccessful: false,
    isAssigning: false,
    isAssignSuccess: false,
    shareMessage: {},
    uploadMessage: {}
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_ANALYSES:
            return state.merge({
                isFetching: false,
                isSuccessful: false
            }).toJS();
        case Constants.UPLOADING_PROJECT_DATA:
            return state.merge({
                isFetching: true,
                isSuccessful: false
            }).toJS();
        case Constants.UPLOADING_PROJECT_DATA_SUCCESSFUL:
            return state.merge({
                isFetching: false,
                isSuccessful: true
            }).toJS();
        case Constants.ERROR_UPLOADING_PROJECT_DATA:
            return state.merge({
                isFetching: false,
                uploadMessage: {error: action.message.message}
            }).toJS();
        case Constants.SHARING_PROJECT:
            return state.merge({
                shareMessage: {}
            }).toJS();
        case Constants.COMPLETE_PROJECT_SHARE:
            return state.merge({
                shareMessage: action.message
            }).toJS();
        case Constants.CLEAR_MESSAGES:
            return state.merge({
                shareMessage: {},
                uploadMessage: {},
                isAssigning: false,
                isAssignSuccess: false
            }).toJS();
        default:
          return state.toJS();
    }
}

module.exports = update;

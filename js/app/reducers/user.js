var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    displayedControls: [],
    requireRedraw: false,
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.MERGE_PLOTS:
            state = state.deleteIn(
                [
                    "displayedControls",
                    action.source
                ]
            );
            state = state.set("requireRedraw", true);
            return state.toJS();

        case Constants.DELETE_PLOT:
            state = state.deleteIn(
                [
                    "displayedControls",
                    action.plot,
                ]
            );
            return state.toJS();

        case Constants.DELETE_ROW:
            if (action.controlName) {
                state = state.set("requireRedraw", true);
            }
            state = state.deleteIn(
                [
                    "displayedControls",
                    action.plot,
                    action.controlIndex
                ]
            );
            return state.toJS();

        case Constants.APPEND_COLUMN:
            let dc = state.get('displayedControls');
            dc = dc.push([]);
            state = state.setIn(
                [
                    'displayedControls',
                ],
                dc
            );
            return state.toJS();

        case Constants.SET_CONTROL:
            state = state.setIn(
                [
                    'displayedControls',
                    action.plotIndex,
                    action.controlIndex,
                ],
                {
                    name: action.controlName,
                    type: action.controlType,
                    value: action.value,
                }
            );
            return state.toJS();

        case Constants.CLEAR_ANALYSIS:
            return state.merge({
                "displayedControls": []
            }).toJS();

        case Constants.RECEIVE_TRANSFORMED_DATA:
            return state.merge({
                requireRedraw: true
            }).toJS();

        case Constants.RECEIVE_SAVE:
            if (action.analysisSave) {
                return Immutable.fromJS(action.analysisSave.configuration.userState).merge({
                    requireRedraw: true
                }).toJS();
            } else {
                return Immutable.fromJS(initialState).merge({
                    requireRedraw: true
                }).toJS();
            }

        case Constants.REPORT_RECEIVE:
            const displayedControls = [];
            action.report.report_items.forEach(item => {
                if (item.object_type === "AnalysisSave") {
                    const colKeys = item.object_data.configuration.analysis.plotCols;
                    Object.keys(colKeys).forEach(key => {
                        displayedControls[Number(key)+item.plotPrefix] = item.object_data.configuration.userState.displayedControls[key];
                    });
                }
            });
            return state.set("displayedControls", displayedControls).toJS();

        case Constants.PLOT_DRAWN:
            return state.merge({
                requireRedraw: action.redraw,
            }).toJS();
        default:
          return state.toJS();
    }
};

module.exports = update;

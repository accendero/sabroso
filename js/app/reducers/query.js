var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    filters: {},
    args: {},
    configArgs: {},
    saveReady: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }

    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.DELETE_ROW:
            if (action.controlName) {
                state = state.deleteIn(["filters", String(action.plot), action.controlName]);
                state = state.deleteIn(["args", String(action.plot), action.controlName]);
                state = state.deleteIn(["configArgs", String(action.plot), action.controlName]);
            }
            return state.toJS();

        case Constants.DELETE_PLOT:
            state = state.deleteIn(["filters", String(action.plot)]);
            state = state.deleteIn(["args", String(action.plot)]);
            state = state.deleteIn(["configArgs", String(action.plot)]);
            return state.toJS();

        case Constants.MERGE_PLOTS:
            state = state.deleteIn(["filters", String(action.source)]);
            state = state.deleteIn(["args", String(action.source)]);
            state = state.deleteIn(["configArgs", String(action.source)]);
            return state.toJS();

        case Constants.RECEIVE_QUERY:
            if (action.query) {
                state = state.set("filters", action.query.filters);
                state = state.set("args", action.query.args);
                state = state.set("configArgs", action.query.configArgs);
                state = state.set("saveReady", true);
                return state.toJS();
            } else {
                return Immutable.fromJS(initialState).set("saveReady", true).toJS();
            }

        case Constants.REPORT_RECEIVE:
            let filters = {},
                args = {},
                configArgs = {},
                colKeys;
            action.report.report_items.forEach(item => {
                if (item.object_type === "AnalysisSave") {
                    colKeys = item.object_data.configuration.analysis.plotCols;
                    Object.keys(colKeys).forEach(key => {
                        const _key = Number(key)+item.plotPrefix;
                        const _query = item.object_data.configuration.query;
                        filters[_key] = _query.filters[key];
                        args[_key] = _query.args[key];
                        configArgs[_key] = _query.configArgs[key];
                    });
                }
            });
            state = state.set("filters", filters);
            state = state.set("args", args);
            state = state.set("configArgs", configArgs);
            return state.set("saveReady", true).toJS();

        case Constants.CLEAR_ANALYSIS:
            var ret = state.merge({
                saveReady: false,
                filters: {},
                args: {},
                configArgs: {}
            });
            return ret.toJS();

        default:
            return state.toJS();
    }
};

module.exports = update;

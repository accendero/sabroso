var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    analysisTypes: {},
    isFetching: false
};

var update = function(state, action) {
    let defaultState = state;
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_ALL_ANALYSIS_CONFIGURATION:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_ALL_ANALYSIS_CONFIGURATION:
            let analysisTypes = {};
            action.analysisTypes.forEach(at => {
              analysisTypes[at.name] = at;
            });
            return state.merge({
                isFetching: false,
                analysisTypes: analysisTypes
            }).toJS();
        default:
          if (defaultState) {
            return defaultState;
          } else {
            return state.toJS();
          }
    }
}

module.exports = update;

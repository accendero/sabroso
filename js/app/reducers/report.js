import Immutable from 'immutable';
import Constants from '../constants';

const initialState = {
    isFetching: false,
    error: null,
    report: {},
    addSave: {value: null},
    addText: '',
};

const _mapped = {
    [Constants.REPORT_REQUEST]: (state, action) => {
        state = Immutable.fromJS(state);
        return state.merge({isFetching: true}).toJS();
    },
    [Constants.REPORT_RECEIVE]: (state, action) => {
        state = Immutable.fromJS(state);
        return state.merge({
            isFetching: false,
            report: action.report,
        }).toJS();
    },
    [Constants.REPORT_ERROR]: (state, action) => {
        state = Immutable.fromJS(state);
        return state.merge({
            isFetching: false,
            report: {},
            error: action.error,
        });
    },
    [Constants.MOVE_REPORT_ITEM]: (state, action) => {
        state = Immutable.fromJS(state);
        const items = state.get('report').get('report_items').toJS().map(i => {
            if (i.id === action.item.id) {
                return action.item;
            }
            return i;
        }).sort((a, b) => {
            if (a.order > b.order) {
                return 1;
            }
            if (a.order < b.order) {
                return -1;
            }
            if (a.id > b.id) {
                return 1;
            }
            if (a.id < b.id) {
                return -1;
            }
            return 0;
        });
        const report = Object.assign({},
            state.get('report').toJS(), {report_items: items});
        return state.merge({report: report}).toJS();
    },
    [Constants.REMOVE_REPORT_ITEM]: (state, action) => {
        state = Immutable.fromJS(state);
        const items = state.get('report').get('report_items').toJS()
            .filter(i => i.id !== action.id);
        const report = Object.assign({}, state.get('report').toJS(),
            {report_items: items});
        return state.merge({report: report}).toJS();
    },
    [Constants.REPORT_SET_SAVE_SELECTION]: (state, action) => {
        state = Immutable.fromJS(state);
        return state.merge({addSave: action.addSave}).toJS();
    },
    [Constants.REPORT_SET_TEXT]: (state, action) => {
        state = Immutable.fromJS(state);
        return state.merge({addText: action.addText}).toJS();
    },
};

export default (state=initialState, action={}) => {
    const _f = _mapped[action.type];
    return _f ? _f(state, action) : state;
};

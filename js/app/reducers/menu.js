var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    isErrorFetching: false,
    projects: [],
    analyses: [],
    reports: [],
    project_saves: [],
    saves: [],
    projectsOfType: [],
    projectsOfBucket: [],
    project: null,
    analysis: null,
    analysisSave: null
};

var update = function(state, action) {
    let defaultState = state;
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_PROJECTS:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_PROJECTS:
            return state.merge({
                isFetching: false,
                projects: action.projects,
            }).toJS();
        case Constants.REQUEST_PROJECTS_OF_TYPE:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_PROJECTS_OF_TYPE:
            return state.merge({
                isFetching: false,
                projectsOfType: action.projects,
            }).toJS();
        case Constants.REQUEST_PROJECTS_OF_BUCKET:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_PROJECTS_OF_BUCKET:
            return state.merge({
                isFetching: false,
                projectsOfBucket: action.projects,
            }).toJS();
        case Constants.REQUEST_ANALYSES:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_ANALYSES:
            return state.merge({
                isFetching: false,
                analyses: action.analyses,
                project: action.project
            }).toJS();
        case Constants.REQUEST_SAVES:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_SAVES:
            return state.merge({
                isFetching: false,
                saves: action.saves,
                analysis: action.analysis,
                project: action.project
            }).toJS();

        // added for saves by project for viz builder
        case Constants.REQUEST_PROJECT_SAVES:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_PROJECT_SAVES:
            return state.merge({
                isFetching: false,
                project_saves: action.saves,
            }).toJS();

        case Constants.REQUEST_SAVE:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_SAVE:
            return state.merge({
                isFetching: false,
                analysisSave: action.analysisSave
            }).toJS();

        case Constants.REQUEST_REPORTS:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_REPORTS:
            return state.merge({
                isFetching: false,
                reports: action.reports,
            }).toJS();
        case Constants.ERROR_RECEIVE_MENU:
            return state.merge({
                isErrorFetching: true,
                isFetching: false,
            }).toJS();
        default:
          if (defaultState) {
            return defaultState;
          } else {
            return state.toJS();
          }
    }
}

module.exports = update;

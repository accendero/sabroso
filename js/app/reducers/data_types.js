var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    dataTypes: [],
    isFetching: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_DATA_TYPES:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_DATA_TYPES:
            return state.merge({
                isFetching: false,
                dataTypes: action.dataTypes
            }).toJS();
        default:
          return state.toJS();
    }
};

module.exports = update;

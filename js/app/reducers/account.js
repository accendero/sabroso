var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    isPasswordChangeError: 1,
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.PASSWORD_CHANGE_NOMATCH:
            return state.merge({
                isFetching: false,
                isPasswordChangeError: true,
                message: action.message,
            }).toJS();
        case Constants.PASSWORD_CHANGE:
            return state.merge({
                isFetching: true,
                isPasswordChangeError: false,
                message: action.message,
            }).toJS();
        case Constants.PASSWORD_CHANGED:
            return state.merge({
                isFetching: false,
                isPasswordChangeError: false,
                message: action.message,
            }).toJS();
        case Constants.PASSWORD_CHANGE_ERROR:
            return state.merge({
                isFetching: false,
                isPasswordChangeError: true,
                message: action.message,
            }).toJS();

        default:
            return state.toJS();
    }
}

module.exports = update;

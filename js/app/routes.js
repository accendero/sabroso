import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

/** Views **/
import App from 'components/App';
import Home from 'components/Home';
import Project from 'components/Project';
import Analysis from 'components/Analysis';
import User from 'components/User';
import Report from 'components/Report';

/** Site components **/
import Page404 from 'components/page404';

/** Site Application root **/
const publicPath = '/';

export default class Routes extends Component {
  render() {
    return (
      <Router forceRefresh={false}>
        <App>
          <Switch>
            <Route exact path={`${ publicPath }project/:projectId`} component={Project} />
            <Route exact path={`${ publicPath }analysis/:analysisId/save/:saveId`} component={Analysis} />
            <Route exact path={`${ publicPath }analysis/:analysisId`} component={Analysis} />
            <Route exact path={`${publicPath}report`} component={Report} />
            <Route exact path={`${publicPath}report/:id`} component={Report} />
            <Route exact path={`${ publicPath }user`} component={User} />
            <Route exact path={publicPath} component={Home} />
            <Route exact path="/" component={Home} />
            <Route component={Page404} />
          </Switch>
        </App>
      </Router>
    );
  }
}

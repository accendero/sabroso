var Constants = {

    REQUEST_TOKEN: "REQUEST_TOKEN",
    RECEIVE_TOKEN: "RECEIVE_TOKEN",
    ERROR_RECEIVE_TOKEN: "ERROR_RECEIVE_TOKEN",
    LOGOUT_USER: "LOGOUT_USER",
    ERROR_RECEIVE_MENU: "ERROR_RECEIVE_MENU",
    REQUEST_PROJECTS: "REQUEST_PROJECTS",
    RECEIVE_PROJECTS: "RECEIVE_PROJECTS",
    SHARING_PROJECT: "SHARING_PROJECT",
    COMPLETE_PROJECT_SHARE: "COMPLETE_PROJECT_SHARE",
    REQUEST_ANALYSES: "REQUEST_ANALYSES",
    RECEIVE_ANALYSES: "RECEIVE_ANALYSES",
    REQUEST_SAVES: "REQUEST_SAVES",
    RECEIVE_SAVES: "RECEIVE_SAVES",
    REQUEST_SAVE: "REQUEST_SAVE",
    RECEIVE_SAVE: "RECEIVE_SAVE",
    RECEIVE_QUERY: "RECEIVE_QUERY",
    START_SHARE_ANALYSIS: "START_SHARE_ANALYSIS",
    COMPLETE_ANALYSIS_SHARE: "COMPLETE_ANALYSIS_SHARE",
    REQUEST_ANALYSIS_CONFIGURATION: "REQUEST_ANALYSIS_CONFIGURATION",
    RECEIVE_ANALYSIS_CONFIGURATION: "RECEIVE_ANALYSIS_CONFIGURATION",
    REQUEST_ALL_ANALYSIS_CONFIGURATION: "REQUEST_ALL_ANALYSIS_CONFIGURATION",
    RECEIVE_ALL_ANALYSIS_CONFIGURATION: "RECEIVE_ALL_ANALYSIS_CONFIGURATION",
    REQUEST_TRANSFORMED_DATA: "REQUEST_TRANSFORMED_DATA",
    RECEIVE_TRANSFORMED_DATA: "RECEIVE_TRANSFORMED_DATA",
    ERROR_RECEIVE_TRANSFORMED_DATA: "ERROR_RECEIVE_TRANSFORMED_DATA",
    REQUEST_DATA_TYPES: "REQUEST_DATA_TYPES",
    RECEIVE_DATA_TYPES: "RECEIVE_DATA_TYPES",
    REQUEST_CONTROL: "REQUEST_CONTROL",
    RECEIVE_CONTROL: "RECEIVE_CONTROL",
    FETCH_CONTROL: "FETCH_CONTROL",
    UPLOADING_PROJECT_DATA: "UPLOADING_PROJECT_DATA",
    UPLOADING_PROJECT_DATA_SUCCESSFUL: "UPLOADING_PROJECT_DATA_SUCCESSFUL",
    ERROR_UPLOADING_PROJECT_DATA: "ERROR_UPLOADING_PROJECT_DATA",
    PLOT_DRAWN: "PLOT_DRAWN",
    PASSWORD_CHANGE: "PASSWORD_CHANGE",
    PASSWORD_CHANGED: "PASSWORD_CHANGED",
    PASSWORD_CHANGE_ERROR: "PASSWORD_CHANGE_ERROR",
    PASSWORD_CHANGE_NOMATCH: "PASSWORD_CHANGE_NOMATCH",
    REQUEST_USERS: "REQUEST_USERS",
    RECEIVE_USERS: "RECEIVE_USERS",
    CLEAR_MESSAGES: "CLEAR_MESSAGES",
    CLEAR_ANALYSIS: "CLEAR_ANALYSIS",
    REPORT_REQUEST: "REPORT_REQUEST",
    REPORT_RECEIVE: "REPORT_RECEIVE",
    REPORT_ERROR: "REPORT_ERROR",
    REQUEST_REPORTS: "REQUEST_REPORTS",
    RECEIVE_REPORTS: "RECEIVE_REPORTS",
    MOVE_REPORT_ITEM: "MOVE_REPORT_ITEM",
    REMOVE_REPORT_ITEM: "REMOVE_REPORT_ITEM",
    REPORT_SET_SAVE_SELECTION: "REPORT_SET_SAVE_SELECTION",
    REPORT_SET_TEXT: "REPORT_SET_TEXT",
    REQUEST_PROJECT_SAVES: "REQUEST_PROJECT_SAVES",
    RECEIVE_PROJECT_SAVES: "RECEIVE_PROJECT_SAVES",
    REQUEST_BUCKETS: "REQUEST_BUCKETS",
    RECEIVE_BUCKETS: "RECEIVE_BUCKETS",
    COMPLETE_BUCKET_MODIFICATION: "COMPLETE_BUCKET_MODIFICATION",
    RECEIVE_AVAILABLE: "RECEIVE_AVAILABLE",
    APPEND_COLUMN: "APPEND_COLUMN",
    SET_CONTROL: "SET_CONTROL",
    MERGE_PLOTS: "MERGE_PLOTS",
    DELETE_PLOT: "DELETE_PLOT",
    DELETE_ROW: "DELETE_ROW",

    ItemTypes: {
        PROJECT: 'Project',
        ANALYSIS: 'Analysis',
        PROJECT_DATA_TYPE: 'Project Data Type',
        SAVE: 'Save'
    },

    AnalysisPlugins: {
        VEGA: 'Vega',
        D3: 'D3'
    },

    ControlTypes: {
        SELECTION: 'Selection',
        RANGE: 'Range',
        ARGUMENT_SELECTION: 'ArgumentSelection',
        CONFIGURATION_SELECTION: 'ConfigurationSelection'
    },

    api_urls: {
        'base' : 'http://127.0.0.1:5000/v1/',
        'login_base': 'http://127.0.0.1:5000/',
        'suffix': "?format=json",
        'get_token' : {
            'url' : 'api-token-auth/',
            'method' : 'POST'
        },
        'add_project_data': {
            'url': 'project_data/',
            'method': 'POST'
        },
        'get_project_data_list': {
            'url': 'projects/data/list/',
            'method': 'GET'
        },
        'get_project_data': {
            'url': 'project_data/',
            'method': 'GET'
        },
        'add_project_bucket': {
            'url': 'assign_buckets/',
            'method': 'POST'
        },
        'get_projects' : {
            'url' : 'projects/',
            'method' : 'GET'
        },
        'add_project' : {
            'url' : 'projects/',
            'method' : 'POST'
        },
        'share_project': {
            'url': 'project/share/',
            'method': 'POST'
        },
        'remove_project': {
            'url': 'projects/',
            'method': 'DELETE'
        },
        'get_analyses' : {
            'url' : 'analyses/',
            'method' : 'GET'
        },
        'add_analysis' : {
            'url': 'analyses/',
            'method': 'POST'
        },
        'remove_analysis' : {
            'url': 'analyses/',
            'method': 'DELETE'
        },
        'share_analysis': {
            'url': 'analysis/share/',
            'method': 'POST'
        },
        'get_analysis_saves' : {
            'url' : 'analysis_saves/',
            'method' : 'GET'
        },
        'add_analysis_save' : {
            'url': 'analysis_saves/',
            'method': 'POST'
        },
        'remove_analysis_save' : {
            'url': 'analysis_saves/',
            'method': 'DELETE'
        },
        'share_analysis_save': {
            'url': 'analysis_save/share/',
            'method': 'POST'
        },
        'get_analysis_types' : {
            'url' : 'analysis_types/',
            'method' : 'GET'
        },
        'get_data_types' : {
            'url' : 'data_types/',
            'method' : 'GET'
        },
        'pivot': {
            'url': 'pivot/',
            'method': 'POST'
        },
        'config': {
            'url': 'update_configuration/',
            'method': 'POST'
        },
        'available': {
            'url': 'available/',
            'method': 'POST'
        },
        'account_change_password': {
          'url': 'account/password/',
          'method': 'POST',
        },
        'get_users': {
          'url': 'users/',
          'method': 'GET',
        },
        'get_report_items': {
          'url': 'reports/',
          'method': 'GET',
        },
        // projects/<project id>/reports
        'get_project_reports': {
          'url': 'reports/',
          'method': 'GET',
        },
        // project/<project id>/saves
        'get_project_saves': {
          'url': 'saves/',
          'method': 'GET',
        },
        'post_report_item': {
          'url': 'report_items/',
          'method': 'POST',
        },
        'remove_report_item': {
            'url': 'report_items/',
            'method': 'DELETE',
        },
        'put_report_item': {
            'url': 'report_items/',
            'method': 'PUT',
        },
        'post_report': {
          'url': 'reports/',
          'method': 'POST',
        },
        'remove_report': {
          'url': 'reports/',
          'method': 'DELETE',
        },
        'get_buckets': {
            'url': 'buckets/',
            'method': 'GET',
        },
        'add_bucket': {
            'url': 'buckets/',
            'method': 'POST',
        },
    },

    headers: {},

    MODE_CREATE: 0xFF,
    MODE_EDIT: 0xEF,
    MODE_VIEW: 0xEE,
};

module.exports = Constants;

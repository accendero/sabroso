import React, {Component} from 'react';
import D3Chart from './D3Chart';
import VegaChart from './VegaChart';
import ControlsWidget from './ControlsWidget';
import {Col, Row} from "react-bootstrap";
import Constants from '../constants';

export default class PlottingComponent extends Component {

    static propTypes = {
        configuration: React.PropTypes.object,
        isReport: React.PropTypes.bool.isRequired,
        plotType: React.PropTypes.string.isRequired,
        isFetching: React.PropTypes.bool.isRequired,
        requestData: React.PropTypes.func.isRequired,
        plotCols: React.PropTypes.object.isRequired,
        plugin: React.PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        let plotCols,
            d3Charts = [];
        if (this.props.plotCols) {
            plotCols = Object.keys(this.props.plotCols).map((i) => Number(i)).sort();
        } else {
            plotCols = Object.keys(this.props.data).map(d => {
                return [d];
            });
        }

        plotCols.forEach(function (i, j) {
            let chart;
            switch (this.props.plugin) {
                case Constants.AnalysisPlugins.D3:
                    chart = (<D3Chart
                        name={i}
                        plotType={this.props.plotType}
                        columns={this.props.plotCols[String(i)]}
                        configuration={this.props.configuration[j]}
                    />);
                    break;
                case Constants.AnalysisPlugins.VEGA:
                    chart = (<VegaChart
                        name={i}
                        plotType={this.props.plotType}
                        columns={this.props.plotCols[String(i)]}
                    />);
                    break;
                default:
                    chart = (
                        <div className="col-md-12">
                            <h3>No valid plugin installed for
                                "{this.props.pluginType}"</h3>
                        </div>
                    );
                    break;
            }
            d3Charts.push(
                <Row key={i}>
                    <ControlsWidget
                        plot={i}
                        plotType={this.props.plotType}
                        isFetching={this.props.isFetching}
                        requestData={this.props.requestData}
                        plotNumber={j}
                        plotCount={plotCols.length}
                        isReport={this.props.isReport}
                    />
                    {chart}
                </Row>
            );
        }, this);
        return (
            <Row>
                <Col md={10} mdOffset={1}>
                    {d3Charts}
                </Col>
            </Row>
        );
    }
}

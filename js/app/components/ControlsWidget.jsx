import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import Dropdown from './Dropdown';
import ControlRow from './ControlRow';
import {
    deletePlot,
    deleteRow,
    mergePlots,
    setControl
} from '../actions/user_actions';
import {Button} from "react-bootstrap";
import {Column, Row} from "simple-flexbox";

class ControlsWidget extends Component {

    static propTypes = {
        configuration: React.PropTypes.object.isRequired,
        controlOptions: React.PropTypes.array.isRequired,
        plot: React.PropTypes.number.isRequired,
        isFetching: React.PropTypes.bool.isRequired,
        requestData: React.PropTypes.func.isRequired,
        setControls: React.PropTypes.object,
        plotCount: React.PropTypes.number.isRequired,
        plotNumber: React.PropTypes.number.isRequired,
        plotType: React.PropTypes.string.isRequired,
        defaultControlName: React.PropTypes.string.isRequired,
        setControlChoice: React.PropTypes.func.isRequired,
        setControlValue: React.PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this._doSubmit = this._doSubmit.bind(this);
        this._mergePlots = this._mergePlots.bind(this);
        this._removePlot = this._removePlot.bind(this);
        this._removeControl = this._removeControl.bind(this);
        this._addControl = this._addControl.bind(this);
        this._setControlChoice = this._setControlChoice.bind(this);
        this._setControlValue = this._setControlValue.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        const dControlLength = Object.keys(this.props.displayedControls).length !== Object.keys(nextProps.displayedControls).length,
            dConfiguration = this.props.configuration !== nextProps.configuration,
            dPlotNumber = this.props.plotNumber !== nextProps.plotNumber,
            dPlotCount = this.props.plotCount !== nextProps.plotCount,
            dFetching = this.props.isFetching !== nextProps.isFetching;
        let dDisplayed = false;
        if (dControlLength === false) {
            Object.keys(this.props.displayedControls).forEach((i) => {
                if (nextProps.displayedControls[i] !== this.props.displayedControls[i]) {
                    dDisplayed = true;
                }
            });
        }
        return dControlLength || dConfiguration || dPlotNumber || dPlotCount || dFetching || dDisplayed;
    }

    _doSubmit(e) {
        e.preventDefault();
        this.props.requestData(this.props.plot);
    }

    _mergePlots(target) {
        this.props.doMerge(this.props.plot, target[0].value);
    }

    _removePlot(e) {
        e.preventDefault();
        this.props.removePlot(this.props.plot);
    }

    _removeControl(plot, controlIndex, controlName) {
        this.props.removeControl(plot, controlIndex, controlName);
    }

    _pullAllControls() {
        return this.props.displayedControls.map((control, i) => {
            return (
                <ControlRow
                    number={Number(i)}
                    allValues={control.value}
                    isReport={this.props.isReport}
                    plot={this.props.plot}
                    isFetching={this.props.isFetching}
                    setControlValue={this._setControlValue}
                    selectOption={this._setControlChoice}
                    allOptions={this.props.controlOptions}
                    control={this.props.configuration[control.name]}
                    removeRow={this._removeControl}
                    key={this.props.plot + "-" + i + "-control-row"}
                />
            );
        });
    }

    _addControl() {
        this.props.newControl(
            this.props.plot,
            this.props.defaultControlName,
            this.props.configuration[this.props.defaultControlName].controlType,
            this.props.displayedControls,
        );
    }

    _setControlChoice(plotIndex, controlIndex, controlName) {
        this.props.setControlChoice(
            plotIndex,
            controlIndex,
            controlName,
            this.props.configuration[controlName].controlType
        )
    }

    _setControlValue(controlIndex, controlName, controlValue) {
        this.props.setControlValue(
            this.props.plot,
            controlIndex,
            controlName,
            this.props.configuration[controlName].controlType,
            controlValue);
    }

    render() {
        if (this.props.isFetching)
            return (<div/>);

        let shownControls = this._pullAllControls(),
            controlOptions = [],
            controlDisabled = true;

        if (this.props.plotCount) {
            controlDisabled = false;
            for (let i = 0; i < this.props.plotCount; i++) {
                if (i != this.props.plotNumber) {
                    controlOptions.push({
                        "label": "Plot " + (i + 1),
                        "value": i
                    });
                }
            }
        }

        return (
            <Column id={this.props.plot + "-controls"}
                    justifyContent='space-between'
                    className='controls-widget'
            >
                <div>
                    <label key={`${this.props.plot}-controls-title`}>
                        Plot controls
                    </label>
                </div>
                {!this.props.isReport &&
                <Row key={`${this.props.plot}-controls-add`}
                     justifyContent='start'
                     style={{height: '3em'}}
                >
                    <Button bsStyle="success"
                            onClick={this._addControl}
                    >
                        Add
                    </Button>
                    <Dropdown
                        menuItems={controlOptions}
                        name={this.props.plot + "-merge-menu"}
                        updateFunc={(val) => this._mergePlots(val)}
                        defaultText="Merge data into plot..."
                        multi={false}
                        disabled={controlDisabled}
                        size={"col-md-3"}
                    />
                </Row>
                }
                {shownControls}
                <Row key={this.props.plot + "-controls-submit"}
                     justifyContent='start'
                     style={{height: '3em'}}
                >
                    <Button onClick={this._doSubmit}>
                        Submit query
                    </Button>
                    {!this.props.isReport &&
                    <Button onClick={this._removePlot}>
                        Remove plot
                    </Button>
                    }
                </Row>
            </Column>
        );
    }
}

export default withRouter(connect(
    function (state, props) {
        const configurationArray = state.analysisTypes.analysisTypes[props.plotType] &&
            state.analysisTypes.analysisTypes[props.plotType].controls || [];
        const configuration = configurationArray.reduce((acc, val) => {
            acc[val.name] = val;
            return acc;
        }, {});
        const controlOptions = configurationArray.map(config => ({
            "label": config.title,
            "value": config.name
        }));

        return {
            controlOptions: controlOptions,
            configuration: configuration,
            displayedControls: state.userState.displayedControls[props.plot] || {},
            defaultControlName: controlOptions[0] && controlOptions[0].value || null,
            ...props
        };
    },
    function (dispatch) {
        return {
            newControl: function newControl(plotIndex, controlName, controlType, displayedControls) {
                let newIndex = displayedControls && Object.keys(displayedControls) ? Object.keys(displayedControls).length : 0;
                dispatch(setControl(plotIndex, newIndex, controlName, controlType, []));
            },
            setControlChoice: function setControlChoice(plotIndex, controlIndex, controlName, controlType) {
                dispatch(setControl(plotIndex, controlIndex, controlName, controlType, []));
            },
            setControlValue: (plotIndex, controlIndex, controlName, controlType, value) => {
                dispatch(setControl(plotIndex, controlIndex, controlName, controlType, value))
            },
            removePlot: function removePlot(plot) {
                dispatch(deletePlot(plot));
            },
            removeControl: function removeControl(plot, controlIndex, controlName) {
                dispatch(deleteRow(plot, controlIndex, controlName));
            },
            doMerge: function doMerge(source, target) {
                dispatch(mergePlots(source, target));
            }
        };
    }
)(ControlsWidget));

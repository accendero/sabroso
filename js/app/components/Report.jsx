import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router';
import SelectField from 'react-select';
import {
    Button,
    ButtonGroup,
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Glyphicon,
    Row
} from 'react-bootstrap';
import {
    addReportItem,
    getReportItems,
    putReportItem,
    removeReportItem,
    setReportSaveSelection,
    setReportText
} from '../actions/report';
import {fetchAnalysisSave, fetchSaves} from '../actions/save_actions';
import {PageTitle} from "./PageTitle";
import {Analysis} from "./Analysis";
import {
    fetchAllAnalysisConfiguration,
    fetchAnalysisConfiguration
} from "../actions/analysis_configuration_actions";
import {fetchData} from "../actions/transformer_actions";

class Report extends Component {
    static propTypes = {
        addSave: PropTypes.object,
        addText: PropTypes.string,
        items: PropTypes.array.isRequired,
        loadItems: PropTypes.func.isRequired,
        authHeader: PropTypes.object.isRequired,
        report: PropTypes.object.isRequired,
        saves: PropTypes.array.isRequired,
    };

    static defaultProps = {
        items: [],
        report: {},
        isFetching: true,
        saves: [],
    };

    constructor(props) {
        super(props);
        this.arrangeReportItems = this.arrangeReportItems.bind(this)
        this._updateAddSave = this._updateAddSave.bind(this);
        this._updateText = this._updateText.bind(this);
        this.addSave = this.addSave.bind(this);
        this.addText = this.addText.bind(this);
    }

    componentWillMount() {
        this.props.pullAnalysesConfigs(this.props.authHeader)
    }

    componentDidMount() {
        if (this.props.match.params && this.props.match.params.id) {
            this.props.loadItems(this.props.match.params.id, this.props.authHeader);
        }
    }

    render() {
        if (this.props.isFetching) {
            return (<h3 style={{color: 'white'}}>Loading...</h3>);

        }
        const addSaves = this._getAddAnalysisSave();
        const addText = this._getAddText();
        const items = this._getReportItems();

        const path = [
            {
                name: this.props.report.project.name,
                itemType: "Project",
                id: this.props.report.project.id
            },
            {
                name: this.props.report.name,
                itemType: "Report",
                id: this.props.report.id
            }
        ];

        return (
            <div className="container">
                <Row id="navigation" className="size-seven">
                    <Col md={12}>
                        <PageTitle path={path} username={this.props.username}/>
                    </Col>
                </Row>
                <Row style={{
                    padding: '1em',
                    backgroundColor: 'rgba(255, 255, 255, 1.0)'
                }}>
                    <h1>
                        {this.props.report.name}
                    </h1>
                    <Col md={12}>
                        <Row>
                            <Col md={4} mdOffset={1}>
                                {addSaves}
                            </Col>
                            <Col md={4} mdOffset={1}>
                                {addText}
                            </Col>
                        </Row>
                        <hr/>
                        {items}
                    </Col>
                </Row>
            </div>
        );
    }

    _updateAddSave(value) {
        this.props.updateAddSave(value);
    }

    _updateText(e) {
        this.props.updateAddText(e.target.value);
    }

    addSave() {
        const items = this.props.report.report_items;
        let order;
        if (items.length === 0) {
            order = 0;
        }
        else {
            order = items[items.length - 1].order + 1;
        }
        console.log(order);
        const item = {
            report: this.props.report.id,
            object_id: this.props.addSave.value,
            object_type: "AnalysisSave",
            order: order
        };
        this.props.postReportItem(
            item,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    addText() {
        const items = this.props.report.report_items;
        let order;
        if (items.length === 0) {
            order = 0;
        }
        else {
            order = (items[items.length - 1].order) + 1;
        }
        console.log(order);
        const item = {
            report: this.props.report.id,
            object_id: 0,
            object_type: 'None',
            text: this.props.addText,
            order: order
        };
        this.props.postReportItem(
            item,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    arrangeReportItems(item, direction) {
        const items = this.props.report.report_items;
        const targetIndex = items.indexOf(item) + direction;
        const swapTarget = items[targetIndex];
        const newItem = Object.assign({}, item, {order: swapTarget.order});
        const newSwapItem = Object.assign({}, swapTarget, {order: item.order});

        this.props.moveReportItem(newItem, this.props.postHeader);
        this.props.moveReportItem(newSwapItem, this.props.postHeader);
    }

    _getAddText() {
        return (
            <div>
                <Row>
                    <FormGroup controlId="addTextControl">
                        <ControlLabel>Add text/html</ControlLabel>
                        <FormControl componentClass="textarea"
                                     placeholder="Add text (supports html)"
                                     onChange={this._updateText}
                        />
                    </FormGroup>
                </Row>
                <Row>
                    <Button onClick={this.addText}>
                        Add Text
                    </Button>
                </Row>
            </div>
        );
    }

    _getAddAnalysisSave() {
        return (
            <div>
                <Row>
                    <FormGroup controlId="addAnalysisSaveControl">
                        <ControlLabel>Add Analysis Save</ControlLabel>
                        <SelectField name="add-data-select"
                                     placeholder="Add analysis save..."
                                     value={this.props.addSave}
                                     clearable={this.props.addSave !== null}
                                     options={this.props.saves.map((prop) => ({
                                         label: prop.name,
                                         value: prop.id
                                     }))}
                                     onChange={this._updateAddSave}
                        />
                    </FormGroup>
                </Row>
                <Row>
                    <Button onClick={this.addSave}>
                        Add Analysis Save
                    </Button>
                </Row>
            </div>
        );
    }

    _getReportItems() {
        console.log('Getting Items');
        const report = this.props.report;
        let aIndex = 0;
        const aKeys = Object.keys(this.props.plotCols);


        const items = report.report_items.map((i, j) => {
            if (i.object_type === "AnalysisSave") {
                const data = i.object_data,
                    config = data.analysis.analysis_type,
                    match = {
                        params: {
                            analysisId: data.analysis.id
                        }
                    };

                const plotCol = {[aKeys[aIndex]]: this.props.plotCols[aKeys[aIndex]]};
                aIndex++;

                return (
                    <div className="report-item">
                        <Row key={i.id}>
                            <Col md={2}/>
                            <Col md={8}>
                                <Analysis isReport={true}
                                          isDataLoading={this.props.isDataLoading}
                                          isErrorFetching={this.props.isErrorFetching}
                                          analysisType={config.name}
                                          analysisTypes={{[config.name]: config}}
                                          activeProject={data.analysis.project}
                                          activeAnalysis={data.analysis}
                                          match={match}
                                          availableData={[]}
                                          args={this.props.args}
                                          filters={this.props.filters}
                                          plotCols={plotCol}
                                          queryReady={this.props.queryReady}
                                          fetchAllAnalysis={() => (false)}
                                          pullAvailable={() => ([])}
                                          pullData={this.props.pullData}
                                          fetchSave={() => (false)}
                                          authHeader={this.props.authHeader}
                                          postHeader={this.props.postHeader}
                                          pullUsers={() => ([])}
                                          doClearMessages={() => (false)}
                                          clearAnalysis={() => (false)}
                                          configArgs={this.props.configArgs}
                                          key={`order-${j}`}
                                />
                            </Col>
                            <Col md={2}>
                                <ButtonGroup className={'pull-right'}>
                                    <Button
                                        onClick={() => this.arrangeReportItems(i, -1)}
                                        bsStyle={'warning'}
                                        bsSize={'xs'}
                                        disabled={report.report_items.length === 1 || i.order === 0}
                                    >
                                        <Glyphicon glyph={'chevron-up'}/>
                                    </Button>
                                    <Button
                                        onClick={() => this.arrangeReportItems(i, 1)}
                                        bsStyle={'warning'}
                                        bsSize={'xs'}
                                        disabled={report.report_items.length === 1 || i === report.report_items[report.report_items.length - 1]}
                                    >
                                        <Glyphicon glyph={'chevron-down'}/>
                                    </Button>
                                    <Button
                                        onClick={() => this.props.deleteReportItem(
                                            i.id,
                                            this.props.report.id,
                                            this.props.authHeader,
                                            this.props.postHeader)}
                                        bsStyle={'danger'}
                                        bsSize={'xs'}>
                                        <Glyphicon glyph={'remove'}/>
                                    </Button>
                                </ButtonGroup>
                                <small>{data.name}</small>
                            </Col>
                        </Row>
                    </div>
                );
            }
            else {
                return (
                    <div key={`order-${j}`} className={'report-item'}>
                        <Row key={i.id}>
                            <Col md={10}>
                                <p dangerouslySetInnerHTML={{__html: i.text}}/>
                            </Col>
                            <Col md={2}>
                                <ButtonGroup className={'pull-right'}>
                                    <Button
                                        onClick={() => this.arrangeReportItems(i, -1)}
                                        bsStyle={'warning'}
                                        bsSize={'xs'}
                                        disabled={report.report_items.length === 1 || i === report.report_items[0]}
                                    >
                                        <Glyphicon glyph={'chevron-up'}/>
                                    </Button>
                                    <Button
                                        onClick={() => this.arrangeReportItems(i, 1)}
                                        bsStyle={'warning'}
                                        bsSize={'xs'}
                                        disabled={report.report_items.length === 1 || i === report.report_items[report.report_items.length - 1]}
                                    >
                                        <Glyphicon glyph={'chevron-down'}/>
                                    </Button>
                                    <Button
                                        onClick={() => this.props.deleteReportItem(
                                            i.id,
                                            this.props.report.id,
                                            this.props.authHeader,
                                            this.props.postHeader)}
                                        bsStyle={'danger'}
                                        bsSize={'xs'}>
                                        <Glyphicon glyph={'remove'}/>
                                    </Button>
                                </ButtonGroup>
                            </Col>
                        </Row>
                    </div>
                );
            }
        });

        return items;
    }

}

export default withRouter(connect(
    function (state) {
        const isFetching = (
            state.report.isFetching ||
            undefined === state.report.report ||
            undefined === state.report.report.project ||
            state.menu.isFetching || !state.analysis.plotCols
        );
        const filters = state.userState.displayedControls
            .map(plotControls => {
                return plotControls && plotControls.filter(control => control.type === 'filter')
                    .reduce((controls, control) => {
                        return {
                            ...controls,
                            [control.name]: control.value,
                        }
                    }, {});
            });
        const configArgs = state.userState.displayedControls
            .map(plotControls => {
                return plotControls && plotControls.filter(control => control.type === 'config')
                    .reduce((controls, control) => {
                        return {
                            ...controls,
                            [control.name]: control.value,
                        }
                    }, {});
            });

        return {
            token: state.auth.token,
            menuItems: state.menu.saves,
            activeProject: state.menu.project,
            isFetching: isFetching,
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
            report: state.report.report,
            saves: state.menu.project_saves,
            username: state.auth.user,
            analysisTypes: state.analysisTypes.analysisTypes,
            plotCols: state.analysis.plotCols,
            filters: filters,
            args: state.query.args,
            configArgs: configArgs,
            queryReady: state.query.saveReady,
            isErrorFetching: state.analysisData.isErrorFetching,
            isDataLoading: state.analysisData.isFetching,
            addSave: state.report.addSave,
            addText: state.report.addText,
        };
    },
    function (dispatch) {
        return {
            pullData: function pullData(name, pivotConfiguration, buckets, filters, args, projectId, cols, analysisType, updateConfig, configArgs, postHeader) {
                dispatch(fetchData(
                    name,
                    pivotConfiguration,
                    buckets,
                    filters,
                    args,
                    projectId,
                    cols,
                    analysisType,
                    updateConfig,
                    configArgs,
                    postHeader));
            },
            fetchAllAnalysis: function fetchAllAnalysis(analysisId, authHeader) {
                dispatch(fetchSaves(analysisId, authHeader));
                dispatch(fetchAnalysisConfiguration(analysisId, authHeader));
            },
            loadItems: function loadItems(document_id, authHeader) {
                dispatch(getReportItems(document_id, authHeader));
            },
            postReportItem: function postReportItem(v_id, item, item_type, authHeader, postHeader) {
                dispatch(addReportItem(
                    v_id, item, item_type, authHeader, postHeader
                ));
            },
            deleteReportItem: function deleteReportItem(itemID, reportID, authHeader, postHeader) {
                dispatch(removeReportItem(
                    itemID, reportID, authHeader, postHeader
                ));
            },
            fetchSave: function fetchSave(saveId, authHeader) {
                dispatch(fetchAnalysisSave(saveId, authHeader));
            },
            pullAnalysesConfigs: function pullAnalysesConfigs(authHeader) {
                dispatch(fetchAllAnalysisConfiguration(authHeader))
            },
            moveReportItem: function moveReportItem(item, postHeader, reportID) {
                dispatch(putReportItem(item, postHeader, reportID))
            },
            updateAddSave: (value) => {
                dispatch(setReportSaveSelection(value));
            },
            updateAddText: (value) => {
                dispatch(setReportText(value));
            },
        }
    }
)(Report));

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {removeProject} from '../actions/project_actions';
import {removeAnalysis} from '../actions/analysis_actions';
import {removeAnalysisSave} from '../actions/save_actions';
import {removeReport} from '../actions/report';
import {Button, Col, ListGroup, ListGroupItem, Panel, Row} from "react-bootstrap";

class Item extends Component {
    static propTypes = {
        id: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.number.isRequired,
        ]),
        name: PropTypes.string.isRequired,
        itemType: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        superItemType: PropTypes.string,
        superId: PropTypes.oneOfType([
            PropTypes.string, PropTypes.number
        ]),
        created_on: PropTypes.string,
        addClick: PropTypes.func,
    }

    constructor(props) {
        super(props);
    }

    doClick(e) {
        if (this.props.addClick)
            return this.props.addClick(e);

        let url = '/';
        if (this.props.superItemType && this.props.superId) {
            url = url + this.props.superItemType.toLowerCase() + '/' + this.props.superId + '/';
        }
        url = url + this.props.itemType.toLowerCase() + '/' + this.props.id + '/';
        this.props.history.push(url);
    }

    doDeleteClick() {
        const itemType = this.props.itemType.toLowerCase();
        const message = "Are you sure you wish to delete this " + itemType + "?";
        if (confirm(message)) {
            let props = this.props;
            this.props.doSubmit(this.props.id, props);
        }
    }

    render() {
        let createdOn = this.props.created_on.split('.')[0];

        const panelHeader = (
            <Row>
                <Col md={9}>
                    <strong>{this.props.name}</strong>
                </Col>
                <Col md={3} className={"text-right"}>
                    <Button bsStyle="warning" onClick={() => this.doDeleteClick()}>
                                    <span className="glyphicon glyphicon-remove-sign full-size"
                                          aria-hidden="true">
                                    </span>
                    </Button>
                </Col>
            </Row>
        );

        return (
            <Col md={3}>
                <Panel header={panelHeader} bsStyle="primary">
                    <ListGroup>
                        <ListGroupItem onClick={(e) => this.doClick(e)} className="clickable">
                            <p>{this.props.description}</p>
                            <p>Created on: {createdOn.split('T')[0]}</p>
                        </ListGroupItem>
                        {this.props.shared_by &&
                        <ListGroupItem bsStyle={'info'}><strong>{this.props.shared_by}</strong></ListGroupItem>
                        }
                    </ListGroup>
                </Panel>
            </Col>
        );
    }
}

export default withRouter(connect(
    function (state, ownProps) {
        return {
            id: ownProps.id,
            name: ownProps.name,
            itemType: ownProps.itemType,
            description: ownProps.description,
            superItemType: ownProps.superItemType,
            superId: ownProps.superId,
            created_on: ownProps.created_on,
            activeProject: state.menu.project,
            activeAnalysis: state.menu.analysis,
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
        };
    },
    function (dispatch) {
        return {
            doSubmit: function doSubmit(id, props) {
                switch (props.itemType) {
                    case "Project":
                        dispatch(removeProject(id, props.authHeader, props.postHeader));
                        break;
                    case "Analysis":
                        dispatch(removeAnalysis(id, props.activeProject, props.authHeader, props.postHeader));
                        break;
                    case "Save":
                        dispatch(removeAnalysisSave(id, props.activeAnalysis, props.postHeader, props.authHeader));
                        break;
                    case "Report":
                        dispatch(removeReport(id, props.activeProject.id, props.authHeader, props.postHeader));
                        break;
                }
            }
        };
    }
)(Item));

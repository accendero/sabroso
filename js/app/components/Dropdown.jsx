import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SelectField from 'react-select';

export default class Dropdown extends Component {
    static propTypes = {
        menuItems: PropTypes.array.isRequired,
        name: PropTypes.string.isRequired,
        updateFunc: PropTypes.func.isRequired,
        activeItem: PropTypes.array,
        defaultText: PropTypes.string,
        multi: PropTypes.bool,
        size: PropTypes.string.isRequired,
        notClearable: PropTypes.bool,
        disabled: PropTypes.bool
    };

    static defaultProps = {
        multi: false,
    };

    constructor(props) {
        super(props);
        this._submit = this._submit.bind(this);
    }

    _submit(eventObjects) {
        if (!this.props.multi) {
            eventObjects = [eventObjects];
        }
        this.props.updateFunc(
            eventObjects
        );
    }

    componentDidMount() {
        if (this.props.notClearable && (!this.props.activeItem || this.props.activeItem.length == 0)) {
            this._submit(this.props.menuItems[0]);
        }
    }

    render() {
        let placeholder = this.props.defaultText ? this.props.defaultText : "Choose option(s)",
            activeItem = this.props.activeItem;
        if (this.props.notClearable && (!this.props.activeItem || this.props.activeItem.length === 0) && this.props.menuItems.length) {
            activeItem = [this.props.menuItems[0]];
        }
        if (!this.props.multi && activeItem && activeItem.length) {
            activeItem = activeItem[0];
        } else if (!this.props.multi && activeItem && !activeItem.length) {
            activeItem = null;
        }

        return (
            <div className={this.props.size}>
                <div className="form-group">
                    <SelectField
                        name={this.props.name + "-select"}
                        placeholder={placeholder}
                        value={activeItem}
                        options={this.props.menuItems}
                        onChange={this._submit}
                        multi={this.props.multi}
                        clearable={!this.props.notClearable}
                        disabled={this.props.disabled}
                    />
                </div>
            </div>
        );
    }
}

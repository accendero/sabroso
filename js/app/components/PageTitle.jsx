import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import LogoutButton from './LogoutButton';

class TitleComponent extends Component {
  static propTypes = {
    link: PropTypes.object.isRequired
  }

  render() {
    let url='/';
    if (this.props.link.superItemType && this.props.link.superId) {
        url = url + this.props.link.superItemType.toLowerCase()+'/'+this.props.link.superId + '/';
    }
    url = url + this.props.link.itemType.toLowerCase()+'/'+this.props.link.id+'/';

    let linkText = <p className="mid-size title-text"><strong>{this.props.link.name}</strong></p>

    return (
        <li>
        <Link to={url}>{linkText}</Link>
        </li>
    );
  }
}

class TitleSeperator extends Component {
  render() {
    return (
        <li>
            <span className="glyphicon glyphicon-chevron-right mid-size title-text simulate-a" aria-hidden="true">
            </span>
        </li>
    );
  }
}

export class PageTitle extends Component {
  static propTypes = {
    path: PropTypes.array.isRequired,
    username: PropTypes.string,
    noLink: PropTypes.bool,
  }

  static defaultProps = {
    username: 'unknown',
  }

  constructor(props) {
    super(props);
  }

  render() {
    let {path, username} = this.props;
    username = username ? username : 'unknown';

    let items = [];
    let seperatorKey;
    let componentKey;

    path.forEach(function(link) {
        seperatorKey = "Seperator."+link.name;
        componentKey = "Component."+link.name;
        items.push(<TitleSeperator key={seperatorKey} />);
        items.push(<TitleComponent key={componentKey} link={link} />);
    });

    return (
      <div className="col-md-12">
        <nav className="navbar">
          <ul className="nav navbar-nav">
            <li>
              <Link to={'/'}>
                <span className="glyphicon glyphicon-home mid-size title-text" aria-hidden="true">
                </span>
              </Link>
            </li>
            {items}
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <Link to={'/user'}>
                <span style={{color: "#ffffff"}} className="glyphicon glyphicon-user mid-size">
                &nbsp;
                {username}
                </span>
              </Link>
            </li>
            <li>
              <LogoutButton />
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default {PageTitle};

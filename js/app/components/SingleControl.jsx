import React, {Component} from 'react';
import Dropdown from './Dropdown';

export default class SingleControlWidget extends Component {

    static defaultProps = {
        options: [],
        placeholders: '',
        notClearable: true,
    };

    static propTypes = {
        control: React.PropTypes.object.isRequired,
        controlNumber: React.PropTypes.number,
        disabled: React.PropTypes.bool,
        name: React.PropTypes.string.isRequired,
        size: React.PropTypes.string.isRequired,
        inputType: React.PropTypes.string.isRequired,
        options: React.PropTypes.array,
        onChange: React.PropTypes.func,
        placeholder: React.PropTypes.string,
        value: React.PropTypes.oneOfType([
            React.PropTypes.array,
            React.PropTypes.string,
            React.PropTypes.number
        ]),
        notClearable: React.PropTypes.bool
    };

    constructor(props) {
        super(props);
        this._onChange = this._onChange.bind(this);
    }

    _onChange(e) {
        let value;

        switch (this.props.inputType) {
            case "free-number":
                value = e.target.value;
                break;
            case "dropdown":
                value = e[0] && e[0].value;
                break;
        }

        this.props.onChange(
            this.props.controlNumber,
            value,
        );
    };

    render() {
        switch (this.props.inputType) {
            case "free-number":
                return (
                    <div className={this.props.size}>
                        <input
                            disabled={this.props.disabled}
                            onChange={this._onChange}
                            className="form-control"
                            type="number"
                            id={this.props.name}
                            placeholder={this.props.placeholder}
                            key={this.props.name}
                            value={this.props.value}
                        />
                    </div>
                );

            case "dropdown":
                return (
                    <Dropdown
                        disabled={this.props.disabled}
                        menuItems={this.props.options}
                        name={this.props.name}
                        updateFunc={this._onChange}
                        defaultText={this.props.placeholder}
                        multi={false}
                        size={this.props.size}
                        activeItem={[this.props.value]}
                        notClearable={this.props.notClearable}
                    />
                );

            default:
                return (<div/>)
        }
    }
}

import React, {Component} from 'react';
import SingleControl from './SingleControl';
import {Button, Glyphicon} from "react-bootstrap";
import {Row} from "simple-flexbox";
import Dropdown from "./Dropdown";

export default class ControlRow extends Component {
    static propTypes = {
        allValues: React.PropTypes.array.isRequired,
        number: React.PropTypes.number.isRequired,
        plot: React.PropTypes.number.isRequired,
        isFetching: React.PropTypes.bool.isRequired,
        selectOption: React.PropTypes.func.isRequired,
        setControlValue: React.PropTypes.func.isRequired,
        allOptions: React.PropTypes.array,
        control: React.PropTypes.object,
        removeRow: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this._changeControlType = this._changeControlType.bind(this);
        this._setControlValue = this._setControlValue.bind(this);
        this._removeRow = this._removeRow.bind(this);
    }

    _removeRow(e) {
        e.preventDefault();
        this.props.removeRow(this.props.plot, this.props.number, this.props.control ? this.props.control.name : null);
    }

    _makeClassNameSize(inputSize, textSize) {
        let classNameSize, classTextSize;
        switch (inputSize) {
            case "small":
                classNameSize = "col-xs-1";
                break;
            case "medium":
                classNameSize = "col-xs-2";
                break;
            case "large":
                classNameSize = "col-xs-3";
                break;
        }
        switch (textSize) {
            case "small":
                classTextSize = "col-xs-1";
                break;
            case "medium":
                classTextSize = "col-xs-2";
                break;
            case "large":
                classTextSize = "col-xs-3";
                break;
        }
        return {
            classTextSize: classTextSize,
            classNameSize: classNameSize
        }
    }

    _changeControlType(e) {
        this.props.selectOption(this.props.plot, this.props.number, e[0].value);
    }

    _setControlValue(index, value) {
        const allValues = [...this.props.allValues];
        allValues[index] = value;
        this.props.setControlValue(this.props.number, this.props.control.name, allValues)
    }

    render() {
        let control = this.props.control,
            atts = control ? this._makeClassNameSize(control.inputSize, control.textSize) : {},
            items = [],
            name = `${this.props.plot}-${this.props.number}-control-selection`;

        if (control) {
            for (let i = 0; i < control.inputNumber; i++) {
                if (control.text[i]) {
                    items.push(
                        <label
                            htmlFor={control.name + '-' + i}
                            className={atts.classTextSize + " col-form-label text-center"}
                            key={control.name + '-' + i + '-label'}
                        >
                            {control.text[i]}
                        </label>
                    )
                }
                items.push(
                    <SingleControl
                        name={control.name + '-' + i}
                        inputType={control.inputType}
                        control={control}
                        options={control.options ? control.options[i] : []}
                        onChange={this._setControlValue}
                        placeholder={control.placeholders[i]}
                        size={atts.classNameSize}
                        key={control.name + '-' + i}
                        plot={this.props.plot}
                        rowName={this.props.control.name}
                        controlNumber={i}
                        value={this.props.allValues[i]}
                    />
                )
            }
        }
        return (
            <Row key={`${this.props.plot}-${this.props.number}-control-row`}
                 style={{height: '3em'}}
            >
                {!this.props.isReport &&
                <Button bsStyle="warning" onClick={this._removeRow}>
                    <Glyphicon glyph="remove-sign" className="full-size"/>
                </Button>
                }
                <Dropdown
                    name={name}
                    menuItems={this.props.allOptions}
                    activeItem={this.props.control ? [this.props.control.name] : null}
                    notClearable={true}
                    size="col-xs-2"
                    key={name}
                    disabled={this.props.isReport}
                    updateFunc={this._changeControlType}
                />
                {items}
            </Row>
        )
    }
}

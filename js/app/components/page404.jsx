import React, {Component} from 'react';
import {Panel} from 'react-bootstrap';

export default class Page404 extends React.Component {
  render() {
    const title = "404";
    const text = "Page not found";

    return(
      <Panel header={title} bsStyle="info">
        {text}
      </Panel>
    );
  }
}

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {PageTitle} from './PageTitle';
import ActionMenu from './ActionMenu';
import {fetchProjects} from '../actions/project_actions';
import {Row} from "react-bootstrap";

class Home extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.pullProjects(this.props.authHeader);
    }

    render() {
        return (
            <div className="container">
                <Row id="navigation">
                    <PageTitle path={[]} username={this.props.username}/>
                </Row>
                <Row id="action-menu">
                    <ActionMenu menuItems={this.props.menuItems} itemType={"Project"}/>
                </Row>
            </div>
        );
    }
}

export default connect(
    function (state) {
        return {
            menuItems: state.menu.projects,
            authHeader: state.auth.header,
            username: state.auth.user,
        };
    },
    function (dispatch) {
        return {
            pullProjects: function pullProjects(authHeader) {
                dispatch(fetchProjects(authHeader));
            }
        };
    }
)(Home);

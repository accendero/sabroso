import React, {Component} from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {fetchToken} from '../actions/auth_actions';
import {loadCookieToken} from '../actions/auth_actions';
import {Button} from "react-bootstrap";
// import {logout} from '../actions/auth_actions';

class App extends Component {
    constructor(props) {
      super(props);
      this.state = {
        user: '',
        password: '',
      }
    }

    _submit(e) {
        this.props.pullToken(this.state.user, this.state.password);
    }

    handleChangeUser(event) {
      this.setState({user: event.target.value});
    }

    handleChangePassword(event) {
      this.setState({password: event.target.value});
    }

    componentDidMount() {
      this.props.syncCookie();
    }

    render() {
        if (this.props.token) {
            return (
                <div id="page">
                    { this.props.children }
                </div>
            );
        } else {
            var error_message = <div />
            if(true === this.props.isTokenError) {
              error_message = <h4>Invalid username or password</h4>
            }
            return (
                <div id="page">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-md-offset-3">
                                <div id="log-in-form" className="row">
                                    <h3 className="strong">Log in</h3>
                                    { error_message }
                                    <form>
                                        <div className="form-group">
                                            <label className="sr-only" htmlFor="InputUsername">Username</label>
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    <span className="glyphicon glyphicon-user" aria-hidden="true">
                                                    </span>
                                                </div>
                                                <input type="text" className="form-control" id="InputUsername" placeholder="Username"  value={this.state.user} onChange={(e) => {this.handleChangeUser(e)}}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="sr-only" htmlFor="InputPassword">Password</label>
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    <span className="glyphicon glyphicon-lock" aria-hidden="true"></span>
                                                </div>
                                                <input type="password"
                                                  className="form-control"
                                                  id="InputPassword"
                                                  placeholder="Password"
                                                  value={this.state.password}
                                                  onChange={(e) => {this.handleChangePassword(e)}} />
                                            </div>
                                        </div>
                                        <Button onClick={(e) => {this._submit(e)}}>
                                          Login
                                        </Button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default withRouter(connect(
  function(state) {
    return {
      token: state.auth.token,
      username: state.auth.user,
      isTokenError: state.auth.isTokenError,
    };
  },
  function(dispatch) {
    return {
      pullToken: function pullToken(username, password) {
        dispatch(fetchToken(username, password));
      },
      syncCookie: function syncCookie() {
        dispatch(loadCookieToken());
      },
      logoutUser: function logoutUser() {
        dispatch(logout());
      },
    };
  }
)(App))

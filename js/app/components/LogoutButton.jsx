import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {logoutUser} from '../actions/logout_button_actions';
import {Button} from "react-bootstrap";

class LogoutButton extends Component {
  constructor(props) {
    super(props);
  }

  clickLogout() {
      this.props.logout();
      this.props.history.push('/');
  }

  render() {
    return(
      <Button onClick={() => this.clickLogout()} bsStyle="primary">
        <strong>Logout</strong>
      </Button>
    );
  }
}

export default withRouter(connect(
  function(state) {
      return { };
  },
  function(dispatch) {
    return {
      logout: function logout() {
        dispatch(logoutUser());
      },
    };
  }
)(LogoutButton));

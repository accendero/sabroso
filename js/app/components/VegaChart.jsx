import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as vega from 'vega';
import {plotRedraw} from '../actions/user_actions';

class VegaChart extends Component {
    constructor(props) {
      super(props);
    }

    static propTypes = {
      name: React.PropTypes.number.isRequired,
      configuration: React.PropTypes.object.isRequired,
      plotType: React.PropTypes.string.isRequired,
      data: React.PropTypes.array.isRequired,
      redraw: React.PropTypes.bool.isRequired
    };


    _updateChart() {
        this._drawChart();
        this._view.change(
          'data', vega.changeset()
            .insert(this.props.data)
            .remove(d => true));
        this._view.run();
        this.props.redrawComplete();
    }

    _drawChart() {
        var el = ReactDOM.findDOMNode(this),
            thiz = this,
            renderer = this.props.renderer ? this.props.renderer : "canvas",
            config = this.props.configuration;
        this._view = new vega.View(vega.parse(config))
          .initialize(el) // set parent DOM element
          .renderer(renderer) // set render type (defaults to 'canvas')
          .hover() // enable hover event processing
          .padding({left: 50, right: 50, top: 50, bottom: 50})
          .run(); // update and render the view
    }

    componentDidUpdate() {
      if (this.props.redraw) {
        this._updateChart();
        this.props.redrawComplete();
      }
    }

    render() {
        return (
            <div id={this.props.name}/>
        );
    }
}

export default connect(
  function(state, props) {
      let data = [];
      if (!props.columns) {
          return
      }
      props.columns.forEach(col => {
        if (state.analysisData.cols.hasOwnProperty(props.name)) {
          data = data.concat(state.analysisData.cols[props.name]);
        }
      });
      return {
          name: props.name,
          data: data,
          configuration: state.analysisData.configuration[props.plotType] || {},
          redraw: state.userState.requireRedraw
      };
  },
  function(dispatch) {
      return {
          redrawComplete: function() {
              dispatch(plotRedraw(false));
          }
      };
  }
)(VegaChart);

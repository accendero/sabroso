import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {LoadingIndicator} from './LoadingIndicator';
import Item from './Item';
import {SubmitItem} from './SubmitItem';
import {addProject} from '../actions/project_actions';
import {addAnalysis} from '../actions/analysis_actions';
import {addAnalysisSave} from '../actions/save_actions';
import {addReport} from '../actions/report';
import {fetchAllAnalysisConfiguration} from '../actions/analysis_configuration_actions';
import {Button, Col, Panel, Row} from "react-bootstrap";

function makeTitle(itemType) {
    var title = itemType + 's';
    switch (itemType) {
        case 'Analysis':
            title = 'Analyses';
            break;
    }
    return title;
}

class ActionMenu extends Component {

    static propTypes = {
        menuItems: PropTypes.array.isRequired,
        itemType: PropTypes.string.isRequired,
        activeProject: PropTypes.oneOfType([
            PropTypes.string, PropTypes.number
        ]),
        activeAnalysis: PropTypes.oneOfType([
            PropTypes.string, PropTypes.number
        ]),
        activeAnalysisSave: PropTypes.oneOfType([
            PropTypes.string, PropTypes.number
        ]),
        activeReport: PropTypes.oneOfType([
            PropTypes.string, PropTypes.number
        ]),
        superItemType: PropTypes.string,
        superId: PropTypes.oneOfType([
            PropTypes.string, PropTypes.number
        ]),
        analysisTypes: PropTypes.object,
        isFetching: PropTypes.bool,
        pullAnalysisTypes: PropTypes.func,
    };

    static defaultProps = {
        analysisTypes: [],
    }

    constructor(props) {
        super(props);
        this.state = {
            submitButton: false,
            submitClass: ""
        };
    }

    _toggleSubmit() {
        this.setState({
            submitButton: this.state.submitButton ? false : true,
            submitClass: this.state.submitButton ? "" : "active"
        });
    }

    _doSubmit(item) {
        var objId;
        switch (this.props.itemType) {
            case "Analysis":
                objId = this.props.activeProject;
                break;

            case "Report":
                objId = this.props.activeProject;
                break;

            case "Save":
                objId = this.props.activeAnalysis;
                item.configuration = {
                    "userState": this.props.userState,
                    "query": this.props.query,
                    "analysis": this.props.analysisState
                };
                break;
        }
        this.props.doSubmit(
            item,
            this.props.itemType,
            this.props.postHeader,
            this.props.authHeader,
            objId
        );
        this._toggleSubmit();
    }

    _getExtraOptions() {
        if (this.props.itemType === 'Analysis') {
            const analysisTypes = [];
            Object.keys(this.props.analysisTypes).forEach(key => {
                let at = this.props.analysisTypes[key];
                let activeProjects = at.active_projects;
                if (activeProjects.indexOf(this.props.activeProject) > -1) {
                    analysisTypes.push({
                        label: at.name,
                        value: at.id,
                        type: "analysis_type",
                        title: "Analysis type"
                    });
                }
            }, this);

            return {
                "Analysis type": analysisTypes,
                "Buckets": this.props.buckets.map(bucket => ({
                    label: bucket.name,
                    value: bucket.id,
                    type: "buckets",
                    title: "Buckets"
                })),
            };
        }
    }

    componentDidMount() {
        if (this.props.itemType == "Analysis" || this.props.itemType == "Save") {
            this.props.pullAnalysisTypes(this.props.authHeader);
        }
    }

    render() {
        let itemTitle = makeTitle(this.props.itemType);

        if (this.props.isFetching || this.props.isErrorFetching)
            return (<LoadingIndicator isErrorFetching={this.props.isErrorFetching}/>);

        const items = this.props.menuItems.map(item => {
            let shared_by = null;
            if(item.shared_by && item.shared_by.username)
                shared_by = (item.shared_by.username !== '') ?
                    'Shared by ' + item.shared_by.username : 'Shared with you';
            return (
                <Item key={item.id}
                      shared_by={shared_by}
                      id={item.id}
                      name={item.name}
                      description={item.description}
                      itemType={this.props.itemType}
                      created_on={item.created_on}
                      superItemType={this.props.superItemType}
                      superId={this.props.superId}
                />
            );
        }, this);

        const panelHeader = (
            <Row>
                <Col md={9}>
                <h3>{itemTitle}</h3>
                </Col>
                <Col md={3} className={"text-right"}>
                    <Button onClick={(e) => {this._toggleSubmit()}}  bsStyle="info" className={this.state.submitClass}>
                        <span className="glyphicon glyphicon-plus full-size title-text"
                              aria-hidden="true">
                        </span>
                        New
                    </Button>
                </Col>
            </Row>
        );

        return (
            <Panel header={panelHeader} style={{backgroundColor: '#ffffff'}}>
                <Row>
                    <SubmitItem itemType={this.props.itemType}
                                submitFunction={item => this._doSubmit(item)}
                                show={this.state.submitButton}
                                extraOptions={this._getExtraOptions()}/>

                    {items}
                </Row>
            </Panel>
        );
    }
}

export default connect(
    function (state, ownProps) {
        const isFetching = state.menu.isFetching || state.analysisTypes.isFetching;
        return {
            isErrorFetching: state.menu.isErrorFetching,
            menuIsFetching: state.menu.isFetching,
            isFetching: isFetching,
            menuItems: ownProps.menuItems,
            itemType: ownProps.itemType,
            activeProject: ownProps.activeProject,
            activeAnalysis: ownProps.activeAnalysis,
            postHeader: state.auth.postHeader,
            authHeader: state.auth.header,
            analysisTypes: state.analysisTypes.analysisTypes,
            analysisState: state.analysis,
            userState: state.userState,
            query: state.query,
            buckets: state.buckets.items,
        };
    },
    function (dispatch) {
        return {
            doSubmit: function doSubmit(item, itemType, postHeader, authHeader, objId) {
                switch (itemType) {
                    case "Project":
                        dispatch(addProject(item, postHeader, authHeader));
                        break;
                    case "Analysis":
                        item.project = objId;
                        dispatch(addAnalysis(item, postHeader, authHeader));
                        break;
                    case "Save":
                        item.analysis = objId;
                        if (!item.hasOwnProperty("configuration")) {
                            item.configuration = {};
                        }
                        dispatch(addAnalysisSave(item, postHeader, authHeader));
                        break;
                    case "Report":
                        item.project = objId;
                        dispatch(addReport(item, postHeader, authHeader));
                        break;
                }
            },
            pullAnalysisTypes: function pullAnalysisTypes(authHeader) {
                dispatch(fetchAllAnalysisConfiguration(authHeader));
            }
        };
    }
)(ActionMenu);

import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class RangeWidget extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    updateFunc: PropTypes.func.isRequired,
    minVal: PropTypes.number,
    maxVal: PropTypes.number,
    dropdownKey: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const width = this.props.width ? this.props.width : 200;
    const min_id = this.props.title+"-input-range-min";
    const max_id = this.props.title+"-input-range-max";

    const submitFunctionMin = (event) => {
      this.props.updateFunc(
        this.props.dropdownKey, "min", event.target.valueAsNumber
      );
    };

    const submitFunctionMax = (event) => {
      this.props.updateFunc(
        this.props.dropdownKey, "max", event.target.valueAsNumber
      );
    };

    return (
      <div className="col-xs-6">
        <div className="row">
          <div className="col-xs-12">
            <label htmlFor={min_id}>{this.props.title}</label>
          </div>
        </div>

        <div className="row">
          <div className="col-xs-5">
            <input onChange={submitFunctionMin}
              className="form-control"
              type="number"
              id={min_id}
              placeholder="Min" value={this.props.minVal}
            />
          </div>

          <div className="col-xs-2">
            <h4 className="text-center">-</h4>
          </div>

          <div className="col-xs-5">
            <input onChange={submitFunctionMax}
              className="form-control"
              type="number"
              id={max_id}
              placeholder="Max"
              value={this.props.maxVal}
            />
          </div>
        </div>
      </div>
    );
  }
}

import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import Immutable from 'immutable';
import {plotRedraw} from '../actions/user_actions';
import plotFunctions from '../utils/d3';
import * as d3 from "d3";

class D3Chart extends Component {
    constructor(props) {
      super(props);
      this._context = null;
    }

    static propTypes = {
      name: React.PropTypes.number.isRequired,
      plotType: React.PropTypes.string.isRequired,
      configuration: React.PropTypes.object.isRequired,
      data: React.PropTypes.object.isRequired,
      redraw: React.PropTypes.bool.isRequired
    };

    _setupPlot() {
      var params = Immutable.fromJS(this.props.configuration);
      return plotFunctions[this.props.plotType](params.toJS());
    }

    _drawChart() {
        var el = d3.select(ReactDOM.findDOMNode(this)),
            context = el.select("svg"),
            plot = this._setupPlot();
        if (context.empty()) {
            context = el.append("svg");
        }
        context.selectAll('g').remove();
        plot(this.props.data, context)
    }

    componentDidMount() {
        this._drawChart();
    }

    componentDidUpdate() {
      if (this.props.redraw) {
        this._drawChart();
        this.props.redrawComplete();
      }
    }

    render() {
        return (
            <div id={this.props.name}></div>
        );
    }
}

export default connect(
    function(state, props) {
        let data = {};
        props.columns.forEach(col => {
          if (state.analysisData.cols.hasOwnProperty(props.name)) {
            data[col] = (state.analysisData.cols[props.name]);
          }
        });
        return {
            data: data,
            redraw: state.userState.requireRedraw,
        };
    },
    function(dispatch) {
        return {
            redrawComplete: function() {
                dispatch(plotRedraw(false));
            }
        };
    }
)(D3Chart);

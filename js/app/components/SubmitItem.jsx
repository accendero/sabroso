import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SelectField from 'react-select';
import {Button, Col, FormGroup, Panel, Row} from "react-bootstrap";

export class SubmitItem extends Component {
    static propTypes = {
        itemType: PropTypes.string.isRequired,
        submitFunction: PropTypes.func.isRequired,
        extraOptions: PropTypes.object,
        show: PropTypes.bool
    };

    static defaultProps = {
        extraOptions: {},
    };

    _submit() {
        let baseSubmit = {
            name: this.state.name,
            description: this.state.description
        };
        Object.keys(this.state.other).forEach(function (key) {
            if (this.state.other[key] instanceof Array) {
                baseSubmit[this.state.other[key][0].type] = this.state.other[key].map(i => (i.value));
            }
            else {
                baseSubmit[this.state.other[key].type] = this.state.other[key].value;
            }
        }, this);
        this.props.submitFunction(baseSubmit);
    }

    _updateState(value) {
        if(null === value)
            return (this.setState({other: {}}) || true);

        if (!value instanceof Array && !value.title)
            return (this.setState({other: {}}) || true);

        if (value instanceof Array && (undefined === value[0] || !value[0].title))
            return (this.setState({other: {}}) || true);

        let other = this.state.other;
        // handle multi select
        if (value instanceof Array) {
            other[value[0].title] = value;
        }
        else {
            other[value.title] = value;
        }
        this.setState({
            "other": other
        });
    }

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            other: {}
        };
    }

    _state(e, field) {
        let newstate = {};
        newstate[field] = e.target.value;
        this.setState(newstate);
    }

    _isMulti(field) {
        return field === 'Buckets';
    }

    render() {
        let additionalControls = Object.keys(this.props.extraOptions).map(
            oneOption => (
                <div key={oneOption} className="form-group">
                    <SelectField
                        name={oneOption.toLowerCase().replace(' ', '-') + "-select"}
                        placeholder={"Choose a(n) " + oneOption.toLowerCase() + " ..."}
                        value={this.state.other[oneOption]}
                        clearable={this.state.other[oneOption] !== '' && this.state.other[oneOption] !== null}
                        options={this.props.extraOptions[oneOption]}
                        multi={this._isMulti(oneOption)}
                        onChange={(e) => this._updateState(e)}/>
                </div>
            ), this);

        if (!this.props.show)
            return (<div/>);

        const panelHeader = (
            <Row>
                <Col md={12}>
                    <strong>{"Add " + this.props.itemType.toLowerCase()}</strong>
                </Col>
            </Row>
        );

        return (
            <Col md={3}>
                <Panel header={panelHeader} bsStyle="success">
                    <form>
                        <FormGroup>
                            <label className="sr-only" htmlFor="InputName">Name</label>
                            <input type="text" className="form-control" id="InputName"
                                   placeholder="Name" onChange={(e) => this._state(e, 'name')}
                                   value={this.state.name}/>
                        </FormGroup>
                        <FormGroup>
                            <label className="sr-only"
                                   htmlFor="InputDescription">Description</label>
                            <textarea className="form-control" id="InputDescription"
                                      placeholder="Description"
                                      onChange={(e) => this._state(e, 'description')}
                                      rows={3}
                                      value={this.state.description}/>
                        </FormGroup>

                        {additionalControls}

                        <Row>
                            <Col md={12} className={"text-right"}>
                            <Button bsStyle="success" onClick={() => this._submit()}>
                                Submit
                            </Button>
                            </Col>
                        </Row>
                    </form>
                </Panel>
            </Col>
        );
    }
}

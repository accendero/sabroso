import React, {Component} from 'react';
import {connect} from 'react-redux';
import SelectField from 'react-select';
import {PageTitle} from './PageTitle';
import ActionMenu from './ActionMenu';
import {fetchAnalyses} from '../actions/analysis_actions';
import {fetchReports} from '../actions/report';
import {fetchDataTypes} from '../actions/data_type_actions';
import {addData, listData, assignDataToProject} from '../actions/project_data';
import {shareProject} from '../actions/project_actions';
import {fetchUsers} from '../actions/account_actions';
import {clearMessages} from '../actions/message_actions';
import {addBucket, fetchBuckets, addProjectBucket} from '../actions/bucket_actions';

import * as d3 from 'd3';
import {Button, Col, Panel, Row, Well} from "react-bootstrap";
import {LoadingIndicator} from "./LoadingIndicator";

export class Project extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectBucket: '',
            dataFile: null,
            shareUser: null,
            newProjectBucket: null,
            newBucket: {
                name: '',
                data_type: '',
            },
        };
    }

    _pullData() {
        let reader = new FileReader(),
            _this = this;
        reader.onload = function (e) {
            let contents = e.target.result;
            _this._submitData(
                d3.csvParse(contents).map(function (row) {
                    let ret = {};
                    Object.keys(row).forEach(function (key) {
                        if (row[key] !== "") {
                            if (!isNaN(+row[key])) {
                                ret[key] = +row[key];
                            } else {
                                ret[key] = row[key];
                            }
                        } else {
                            ret[key] = null;
                        }
                    });
                    return ret;
                })
            );
        };
        reader.readAsText(this.state.dataFile);
    }

    _submitData(data) {
        this.props.uploadData(
            data,
            this.props.match.params.projectId,
            this.state.projectBucket.value,
            this.props.postHeader
        );
    }

    _setFile(e) {
        this.setState({
            dataFile: e.target.files[0]
        });
    }

    _updateProjectBucket(value) {
        this.setState({
            projectBucket: value
        });
    }

    _updateNewProjectBucket(value) {
      this.setState({
          newProjectBucket: value
      });
    }

    _updateShareUser(value) {
        this.setState({
            shareUser: value
        });
    }

    _shareProject() {
        this.props.doShareProject(
            this.props.activeProject.id,
            this.state.shareUser.value,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    _updateNewBucketName(e) {
        this.setState({
            newBucket: {
                name: e.target.value,
                data_type: this.state.newBucket.data_type,
                projects: [this.props.activeProject.id]
            }
        });
    }

    _updateNewBucketDataType(e) {
        this.setState({
            newBucket: {
                name: this.state.newBucket.name,
                data_type: e.value,
                projects: [this.props.activeProject.id]
            }
        });
    }

    _newBucket() {
        this.props.doNewBucket(
            this.state.newBucket,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    _addNewProjectBucket() {
        this.props.doProjectBucket(
            this.state.newProjectBucket.value,
            this.props.activeProject.id,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    componentDidMount() {
        this.props.pullAnalyses(
            this.props.match.params.projectId, this.props.authHeader
        );
        this.props.pullReports(
            this.props.match.params.projectId, this.props.authHeader
        );
        this.props.pullDataTypes(this.props.authHeader);
        this.props.pullUsers(this.props.authHeader);
        this.props.pullBuckets(this.props.authHeader);
        this.props.doClearMessages();
    }

    render() {
        let excludeFromShareList = this.props.username;

        let path = [],
            activeProjectId,
            projectBucketList,
            statusStatement = (<div/>),
            assignStatusStatement = (<div/>),
            shareMessage = (<div/>),
            projectBucketOptions = [],
            allBucketOptions = [],
            dataTypeOptions = this.props.dataTypes.map(dataType => ({
                label: dataType.name,
                value: dataType.id
            })),
            projectDataTypeOptions = dataTypeOptions,
            userSelectOptions = this.props.userList.map(uData => ({
                label: uData.username,
                value: uData.id
            })),
            projectId = Number(this.props.match.params.projectId);

        this.props.buckets.forEach(b => {
          let pushObj = {
            label: b.name,
            value: b.id
          };
          if (b.projects.indexOf(projectId) > -1) {
            projectBucketOptions.push(pushObj);
          } else {
            allBucketOptions.push(pushObj);
          }
        });

        projectBucketList = this.props.buckets.map((obj, i) => {
            const linkage = obj.current_user_is_owner ? "Owner" : "Shared";
            return (<div key={obj.id}>{i + 1}. [{linkage}] {obj.name}</div>);
        });

        // remove selected user from share with list
        userSelectOptions = userSelectOptions.filter(function (item) {
            return item.label !== excludeFromShareList;
        });

        if (this.props.activeProject) {
            path = [
                {
                    name: this.props.activeProject.name,
                    itemType: "Project",
                    id: this.props.activeProject.id
                }
            ];
            activeProjectId = this.props.activeProject.id;
        }
        if (this.props.projectData.isFetching) {
            statusStatement = (
                <div className="alert alert-info" role="alert">Fetching...</div>
            );
        } else if (Object.keys(this.props.projectData.uploadMessage).length) {
            statusStatement= (
                <div className="alert alert-danger" role="alert">Upload was unsuccessful.</div>
            );
        } else if (this.props.projectData.isSuccessful) {
            statusStatement = (
                <div className="alert alert-success" role="alert">Successful upload.</div>
            );
        }
        if ('error' in this.props.projectData.shareMessage) {
            shareMessage = (
                <div className="alert alert-danger" role="alert">{this.props.projectData.shareMessage.error}</div>
            );
        }
        else if ('success' in this.props.projectData.shareMessage) {
            shareMessage = (
                <div className="alert alert-success" role="alert">{this.props.projectData.shareMessage.success}</div>
            );
        }
        if ('error' in this.props.bucketMessages) {
            assignStatusStatement = (
                <div className="alert alert-danger" role="alert">{this.props.projectData.shareMessage.error}</div>
            );
        }
        else if ('success' in this.props.bucketMessages) {
            assignStatusStatement = (
                <div className="alert alert-success" role="alert">{this.props.projectData.shareMessage.success}</div>
            );
        }

        return (
            <div className="container">
                <div id="navigation" className="row size-seven">
                    <PageTitle path={path} username={this.props.username}/>
                </div>

                <div id="action-menu" className="row">
                    <ActionMenu
                        menuItems={this.props.menuItems.analyses}
                        itemType={"Analysis"}
                        activeProject={activeProjectId}
                    />
                    <ActionMenu
                        menuItems={this.props.menuItems.reports}
                        itemType={"Report"}
                        activeProject={activeProjectId}
                    />
                </div>

                <div id="page-break" className="row"><br/></div>

                {this.props.isFetching &&
                    <LoadingIndicator isErrorFetching={false}/>
                }

                {!this.props.isFetching &&
                    <Panel id="dashboard" className={"row"}>
                        <Row>
                            <Col md={6} className="form-padding">

                                <Panel bsStyle="primary" header={"Create Bucket"}>
                                    <form>
                                        <div className="form-group">
                                            <input className="form-control" type="text" name="user-create-bucket"
                                                   placeholder="Bucket name..."
                                                   value={this.state.newBucket.name}
                                                   onChange={(e) => this._updateNewBucketName(e)}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <SelectField name="project-data-type-select"
                                                         placeholder="Choose a data type..."
                                                         value={this.state.newBucket.data_type}
                                                         options={dataTypeOptions}
                                                         clearable={this.state.newBucket.data_type !== ''}
                                                         onChange={(e) => this._updateNewBucketDataType(e)}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <Button bsStyle="success" onClick={() => this._newBucket()}>
                                                Create bucket
                                            </Button>
                                        </div>
                                        {shareMessage}
                                    </form>
                                </Panel>

                                <Panel bsStyle="primary" header={"Upload data"}>
                                    <form>
                                        <div className="form-group">
                                            <SelectField name="bucket-select"
                                                         placeholder="Choose a bucket..."
                                                         value={this.state.projectBucket}
                                                         options={projectBucketOptions}
                                                         clearable={this.state.projectBucket !== ''}
                                                         onChange={(e) => this._updateProjectBucket(e)}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="file" className="form-control"
                                                   id="input-item-file" placeholder="File"
                                                   onChange={(e) => this._setFile(e)}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <Button bsStyle="success" onClick={() => this._pullData()}>
                                                Upload
                                            </Button>
                                        </div>
                                        {statusStatement}
                                    </form>
                                </Panel>

                                <Panel bsStyle="primary" header={"Add bucket to project"}>
                                    <form>
                                        <div className="form-group">
                                            <SelectField name="project-bucket-select"
                                                         placeholder="Choose a bucket..."
                                                         value={this.state.newProjectBucket}
                                                         clearable={this.state.newProjectBucket !== ''}
                                                         options={allBucketOptions}
                                                         onChange={(e) => this._updateNewProjectBucket(e)}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <Button bsStyle="success" onClick={() => this._addNewProjectBucket()}>
                                                Use selected
                                            </Button>
                                        </div>
                                        {assignStatusStatement}
                                    </form>
                                </Panel>

                                <Panel bsStyle="primary" header={"Share project"}>
                                    <form>
                                        <div className="form-group">
                                            <SelectField name="user-share-select"
                                                         placeholder="Select user to share project with..."
                                                         value={this.state.shareUser}
                                                         options={userSelectOptions}
                                                         clearable={this.state.shareUser !== ''}
                                                         onChange={(val) => this._updateShareUser(val)}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <Button bsStyle="success" onClick={() => this._shareProject()}>
                                                Share project
                                            </Button>
                                        </div>
                                        {shareMessage}
                                    </form>
                                </Panel>

                            </Col>

                            <Col md={6} className="form-padding">
                                <Panel header={"Data Summary"}>
                                    <Well bsSize={"large"}>
                                        {projectBucketList}
                                    </Well>
                                </Panel>
                            </Col>
                        </Row>
                    </Panel>
                }

            </div>
        );
    }
}

export default connect(
    function (state) {
        const isFetching = (
            state.menu.isFetching
        );
        return {
            isFetching: isFetching,
            menuItems: {
                analyses: state.menu.analyses,
                reports: state.menu.reports
            },
            dataTypes: state.dataTypes.dataTypes,
            activeProject: state.menu.project,
            projectData: state.projectData,
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
            username: state.auth.user,
            userList: state.users.users,
            buckets: state.buckets.items,
            bucketMessages: state.buckets.messages
        };
    },
    function (dispatch) {
        return {
            pullAnalyses: function pullAnalyses(projectId, authHeader) {
                dispatch(fetchAnalyses(projectId, authHeader));
            },
            pullReports: function pullReports(projectId, authHeader) {
                dispatch(fetchReports(projectId, authHeader));
            },
            pullDataTypes: function pullDataTypes(authHeader) {
                dispatch(fetchDataTypes(authHeader));
            },
            uploadData: function uploadData(data, dataType, projectId, authHeader) {
                dispatch(addData(data, dataType, projectId, authHeader));
            },
            pullUsers: function pullUsers(authHeader) {
                dispatch(fetchUsers(authHeader));
            },
            pullBuckets: function pullBuckets(authHeader) {
                dispatch(fetchBuckets(authHeader));
            },
            doShareProject: function doShareProject(projectId, shareUser, authHeader, postHeader) {
                dispatch(shareProject(projectId, shareUser, authHeader, postHeader));
            },
            doClearMessages: function doClearMessages() {
                dispatch(clearMessages());
            },
            doNewBucket: function doNewBucket(b_obj, aHeader, pHeader) {
                dispatch(addBucket(b_obj, aHeader, pHeader));
            },
            doProjectBucket: function doProjectBucket(bid, pid, aHeader, pHeader) {
                dispatch(addProjectBucket(bid, pid, aHeader, pHeader));
            },
        };
    }
)(Project);

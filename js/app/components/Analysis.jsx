import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import Dropdown from './Dropdown';
import SelectField from 'react-select';
import {PageTitle} from './PageTitle';
import ActionMenu from './ActionMenu';
import PlottingWidget from './PlottingWidget';
import {LoadingIndicator} from './LoadingIndicator';
import {
    fetchAnalysisSave,
    fetchSaves,
    shareAnalysisSave,
    wipeActiveSave
} from '../actions/save_actions';
import {fetchAnalysisConfiguration} from '../actions/analysis_configuration_actions';
import {fetchUsers} from '../actions/account_actions';
import {fetchAvailable, fetchData} from '../actions/transformer_actions';
import {clearCurrentAnalysis, shareAnalysis} from '../actions/analysis_actions';
import {clearMessages} from '../actions/message_actions';
import {appendCols} from '../actions/user_actions';
import {Button, Col, Row} from "react-bootstrap";

export class Analysis extends Component {

    static defaultProps = {
        analysisType: '',
        analysisTypes: {},
        transformationConfiguration: {},
        activeProject: {
            id: 0,
        },
        activeAnalysis: {
            id: 0,
        },
        isReport: false,
        reportPrefix: 0
    };

    constructor(props) {
        super(props);
        this.state = {
            shareUser: '',
            userList: [],
            analysisRequested: false,
            availableRequested: false,
            saveRequested: false,
            dataRequested: false,
            addCol: null
        };
        this._requestData = this._requestData.bind(this);
        this._newPlot = this._newPlot.bind(this);
    }

    componentDidMount() {
        this.props.clearAnalysis();
        this._requestAnalysis();
        this.props.pullUsers(this.props.authHeader);
        this.props.doClearMessages();
    }

    componentWillUnmount() {
        this.props.clearAnalysis();
    }

    componentWillReceiveProps(nextProps) {
        if (!Object.keys(nextProps.analysisTypes).length) {
            return nextProps
        }
        if (!nextProps.match.params.saveId && this.props.activeAnalysisSave) {
          this.props.clearAnalysisSave();
        }
        if (nextProps.match.params.saveId !== this.props.match.params.saveId) {
            this.setState({
                analysisRequested: false,
                availableRequested: false,
                saveRequested: false,
                dataRequested: false
            });
            this.props.clearAnalysis();
        }
        if (!this.state.analysisRequested && !this.props.isReport) {
            this._requestAnalysis();
        }
        if (!nextProps.analysisType) {
            return nextProps
        }
        if (!this.state.availableRequested && !this.props.isReport) {
            this._requestAvailable(nextProps);
        }
        if (!this.state.saveRequested && this.props.match.params.saveId && !this.props.isReport) {
            this._requestSave();
        }
        if (!this.state.dataRequested && !nextProps.isDataLoading && Object.keys(nextProps.plotCols).length && this.props.queryReady) {
            this._requestInitialData(nextProps.plotCols);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
      const dMenuItems = this.props.menuItems !== nextProps.menuItems,
            dActiveAnalysis = this.props.activeAnalysis !== nextProps.activeAnalysis,
            dActiveAnalysisSave = this.props.activeAnalysisSave !== nextProps.activeAnalysisSave,
            dAnalysisType = this.props.analysisType !== nextProps.analysisType,
            dIsErrorFetching = this.props.isErrorFetching !== nextProps.isErrorFetching,
            dIsDataLoading = this.props.isDataLoading !== nextProps.isDataLoading,
            dName = this.props.name !== nextProps.name,
            dIsSharing = this.props.isSharing !== nextProps.isSharing,
            dIsDoneSharing = this.props.isDoneSharing !== nextProps.isDoneSharing,
            dShareMessage = this.props.shareMessage !== nextProps.shareMessage,
            dState = this.state !== nextState;
      return (
        dMenuItems || dActiveAnalysis || dActiveAnalysisSave || dAnalysisType ||
        dIsErrorFetching || dIsDataLoading || dName || dIsSharing || dIsDoneSharing ||
        dShareMessage || dState
      );
    }

    _requestInitialData(plotCols) {
        Object.keys(plotCols).forEach(plot => {
            this._requestData(plot, plotCols[plot]);
        });
    }

    _requestSave() {
        this.setState({
            saveRequested: true
        });
        this.props.fetchSave(this.props.match.params.saveId, this.props.authHeader);
    }

    _requestAnalysis() {
        this.setState({
            analysisRequested: true
        });
        this.props.fetchAllAnalysis(
            this.props.match.params.analysisId,
            this.props.authHeader
        );
    }

    _requestAvailable(nextProps) {
        this.setState({
            availableRequested: true
        });
        nextProps.pullAvailable(
            nextProps.analysisTypes[nextProps.analysisType].available_method,
            nextProps.activeAnalysis.buckets,
            nextProps.activeProject.id,
            nextProps.postHeader
        );
    }

    _newPlot() {
        if (!this.state.addCol || this.state.addCol.length === 0) {
            alert('Please select data before adding a plot');
            return;
        }

        let cols = this.state.addCol,
            colNames;
        if (!cols.length) {
            return;
        }
        colNames = cols.map(col => col.value);
        this.props.addNewCols(colNames);
        this._requestData(Object.keys(this.props.plotCols).length, colNames);
    }

    _requestData(plot, cols) {
        console.log('Requesting Data');
        this.setState({
            dataRequested: true
        });
        if (!cols) {
            cols = this.props.plotCols[plot];
        }
        this.props.pullData(
            plot,
            this.props.analysisTypes[this.props.analysisType].transformationConfiguration,
            this.props.activeAnalysis.buckets,
            this.props.filters[plot],
            this.props.args[plot],
            this.props.activeProject.id,
            cols,
            this.props.analysisType,
            this.props.analysisTypes[this.props.analysisType].updateConfiguration,
            this.props.configArgs[plot],
            this.props.postHeader
        );
    }
    _buildAnalysisInsert() {
        console.log('building insert');
        if (this.props.analysisType && this.props.activeProject && this.props.analysisTypes[this.props.analysisType]) {
            let analysisConfig = this.props.analysisTypes[this.props.analysisType];
            return (<PlottingWidget
                isReport={this.props.isReport}
                plotType={this.props.analysisType}
                isFetching={this.props.isDataLoading || this.props.isErrorFetching}
                requestData={this._requestData}
                plotCols={this.props.plotCols}
                plugin={analysisConfig.plugin}
                configuration={this.props.configArgs}
            />);
        } else {
            return (
                <div className="col-md-12">
                    <h3>No analysis type or valid active project selected.</h3>
                </div>
            );
        }
    }

    _updateShareUser(value) {
        this.setState({
            shareUser: value
        });
    }

    _updateAddData(value) {
        this.setState({
            addCol: value
        });
    }

    _shareAnalysis() {
        if (this.state.shareUser.value === null) {
            return false;
        }
        this.props.doShareAnalysis(
            this.props.activeAnalysis.id,
            this.state.shareUser.value,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    _shareAnalysisSave() {
        if (this.state.shareUser.value === null) {
            return false;
        }
        this.props.doShareAnalysisSave(
            this.props.activeAnalysisSave.id,
            this.state.shareUser.value,
            this.props.authHeader,
            this.props.postHeader
        );
    }

    render() {
        let path = [];
        let analysisInsert = this._buildAnalysisInsert();
        let actionMenuDiv = (<div/>);
        let activeSave;
        let superItemType;
        let loadingIndicatorDiv = (<div/>);
        let superId;
        let analysisShareMessage = (<div/>);

        if (this.props.activeProject && this.props.activeAnalysis) {
            superId = this.props.activeAnalysis.id;
            superItemType = "Analysis";
            if (this.props.activeAnalysisSave) {
                activeSave = this.props.activeAnalysisSave.id;
            }
            actionMenuDiv = (
                <ActionMenu
                    menuItems={this.props.menuItems} itemType={"Save"}
                    activeProject={this.props.activeProject.id}
                    activeAnalysis={this.props.activeAnalysis.id}
                    activeSave={activeSave} superItemType={superItemType}
                    superId={superId}/>
            );
            path = [
                {
                    name: this.props.activeProject.name,
                    itemType: "Project",
                    id: this.props.activeProject.id
                },
                {
                    name: this.props.activeAnalysis.name,
                    itemType: "Analysis",
                    id: this.props.activeAnalysis.id
                }
            ];
            if (this.props.activeAnalysisSave) {
                path.push({
                    name: this.props.activeAnalysisSave.name,
                    itemType: "Save",
                    id: this.props.activeAnalysisSave.id,
                    superItemType: "Analysis",
                    superId: this.props.activeAnalysis.id
                });
            }
        }

        const excludeFromShareList = this.props.username;
        let userSelectOptions = this.props.isReport ? [] : this.props.userList.map(
            u => ({label: u.username, value: u.id})
        );

        // remove selected user from share with list
        userSelectOptions = userSelectOptions.filter(
            i => (i.label !== excludeFromShareList)
        );

        let shareAnalysisOrSave = (
            <div className="form-group">
                <Button onClick={() => {
                    this._shareAnalysis()
                }}>
                    Share Analysis
                </Button>
            </div>
        );

        if (this.props.activeAnalysisSave) {
            shareAnalysisOrSave = (
                <div className="form-group">
                    <Button onClick={() => {
                        this._shareAnalysisSave()
                    }}>
                        Share Analysis Save
                    </Button>
                </div>
            );
        }

        if (this.props.isDoneSharing) {
            if ('error' in this.props.shareMessage) {
                analysisShareMessage = (
                    <div className="alert alert-danger" role="alert">
                        {this.props.shareMessage.error}
                    </div>
                );
            }
            else if ('success' in this.props.shareMessage) {
                analysisShareMessage = (
                    <div className="alert alert-success" role="alert">
                        {this.props.shareMessage.success}
                    </div>
                );
            }
        }
        else if (this.props.isSharing) {
            analysisShareMessage = (
                <div className="alert alert-info" role="alert">Sharing...</div>
            );
        }

        if (this.props.isDataLoading || this.props.isErrorFetching) {
            loadingIndicatorDiv = (
                <LoadingIndicator isErrorFetching={this.props.isErrorFetching} id="vega-widget"/>
            );
        }

        const innerContent = (
          <Row>
            <Col md={12}>
              {!this.props.isReport &&
              <div>
                  <div id="navigation" className="row size-seven">
                      <PageTitle path={path} username={this.props.username}/>
                  </div>
                  <div id="action-menu" className="row">
                      {actionMenuDiv}
                  </div>
                  <div id="page-break" className="row">
                      <br/>
                  </div>
              </div>
              }

              <Row id="dashboard" style={{backgroundColor: '#ffffff'}}>
                  <Col md={12}>
                      <Row>
                          {loadingIndicatorDiv}
                      </Row>

                      {!this.props.isReport &&
                      <Row>
                        <form>
                          <Dropdown name="add-data-select"
                                       defaultText="Add plot using data..."
                                       activeItem={this.state.addCol}
                                       multi={true}
                                       menuItems={this.props.availableData.map((prop) => ({
                                           label: prop[1],
                                           value: prop[0]
                                       }))}
                                       size="form-padding col-md-3"
                                       updateFunc={(val) => {
                                           this._updateAddData(val)
                                       }}/>
                          <div className="form-padding col-md-2">
                            <div className="form-group">
                                <Button onClick={() => {
                                    this._newPlot()
                                }}>
                                    Add plot
                                </Button>
                            </div>
                          </div>
                        </form>
                      </Row>
                      }
                      <Row>
                          {analysisInsert}
                      </Row>
                      {!this.props.isReport &&
                      <Row>
                          <Col md={4} className={"form-padding"}>
                              <form>
                                  <div className="form-group">
                                      <SelectField name="user-share-select"
                                                   placeholder="Select user to share project with..."
                                                   value={this.state.shareUser}
                                                   clearable={this.state.shareUser !== ''}
                                                   options={userSelectOptions}
                                                   onChange={(val) => {
                                                       this._updateShareUser(val)
                                                   }}/>
                                  </div>
                                  {shareAnalysisOrSave}
                                  {analysisShareMessage}
                              </form>
                          </Col>
                      </Row>
                      }
                  </Col>
                </Row>
            </Col>
          </Row>
        );

        let outerContent = innerContent;
        if (!this.props.isReport) {
          outerContent = (
            <div className="container">
              {innerContent}
            </div>
          );

        }

        return outerContent;
    }
}

export default withRouter(connect(
    function (state) {
        const filters = state.userState.displayedControls
            .map(plotControls => {
                return plotControls.filter(control => control.type === 'filter')
                    .reduce((controls, control) => {
                    return {
                        ...controls,
                        [control.name]: control.value,
                    }
                }, {});
            });
        const configArgs = state.userState.displayedControls
            .map(plotControls => {
                return plotControls.filter(control => control.type === 'config')
                    .reduce((controls, control) => {
                    return {
                        ...controls,
                        [control.name]: control.value,
                    }
                }, {});
            });

        return {
            menuItems: state.menu.saves,
            activeProject: state.menu.project,
            activeAnalysis: state.menu.analysis,
            activeAnalysisSave: state.menu.analysisSave,
            analysisType: state.analysis.analysisType,
            analysisTypes: state.analysisTypes.analysisTypes,
            name: state.analysis.name,
            isSharing: state.analysis.isSharing,
            isDoneSharing: state.analysis.isDoneSharing,
            shareMessage: state.analysis.shareMessage,
            availableData: state.analysisData.available,
            plotCols: state.analysis.plotCols,
            filters: filters,
            args: state.query.args,
            configArgs: configArgs,
            queryReady: state.query.saveReady,
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
            username: state.auth.user,
            userList: state.users.users,
            isErrorFetching: state.analysisData.isErrorFetching,
            isDataLoading: state.analysisData.isFetching
        };
    },
    function (dispatch) {
        return {
            fetchAllAnalysis: function fetchAllAnalysis(analysisId, authHeader) {
                dispatch(fetchSaves(analysisId, authHeader));
                dispatch(fetchAnalysisConfiguration(analysisId, authHeader));
            },
            fetchSave: function fetchSave(saveId, authHeader) {
                dispatch(fetchAnalysisSave(saveId, authHeader));
            },
            pullUsers: function pullUsers(authHeader) {
                dispatch(fetchUsers(authHeader));
            },
            doShareAnalysis: function doShareAnalysis(analysisId, shareUser, authHeader, postHeader) {
                dispatch(shareAnalysis(analysisId, shareUser, authHeader, postHeader));
            },
            doShareAnalysisSave: function doShareAnalysisSave(analysisSaveId, shareUser, authHeader, postHeader) {
                dispatch(shareAnalysisSave(analysisSaveId, shareUser, authHeader, postHeader));
            },
            doClearMessages: function doClearMessages() {
                dispatch(clearMessages());
            },
            clearAnalysis: function clearAnalysis() {
                dispatch(clearCurrentAnalysis());
            },
            pullData: function pullData(name, pivotConfiguration, buckets, filters, args, projectId, cols, analysisType, configFunc, configArgs, postHeader) {
                dispatch(fetchData(name, pivotConfiguration, buckets, filters, args, projectId, cols, analysisType, configFunc, configArgs, postHeader));
            },
            pullAvailable: function pullAvailable(available, buckets, projectId, postHeader) {
                dispatch(fetchAvailable(available, buckets, projectId, postHeader));
            },
            addNewCols: function addNewCols(cols) {
                dispatch(appendCols(cols));
            },
            clearAnalysisSave: function clearAnalysisSave() {
                dispatch(wipeActiveSave());
            }
        };
    }
)(Analysis));

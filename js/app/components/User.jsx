import React, {Component} from 'react';
import {connect} from 'react-redux';
import {changePassword} from '../actions/account_actions';
import {PageTitle} from './PageTitle';
import {Button} from "react-bootstrap";

export class User extends Component {
  static defaultProps = {
    password: '',
    password2: '',
  };

  _submitChangePassword() {
    this.props.changePasswordUser(
      this.props.postHeader,
      this.state.password,
      this.state.password2
    );
  }

  handleChange(e, item) {
    let state = {};
    state[item] = e.target.value;
    this.setState(state);
  }

  render() {
    let path = [
      {
        name: "User",
        itemType: "User",
        id: this.props.username,
        noLink: true,
      }
    ];

    let feedback_message = <div />
    if(null !== this.props.message) {
      feedback_message = <h3>{this.props.message}</h3>;
    }

    return (
      <div className="container">
        <div id="navigation" className="row size-seven">
          <PageTitle path={path} username={this.props.username} />
        </div>

      <div className="row">
        <div className="col-md-12 functional-block">
          <div className="navbar">
            <h4 className="subtitle navbar-text">
              <strong>Change password</strong>
            </h4>
          </div>
          <div className="row">
            <div className="col-md-4">
            { feedback_message }
            <form>
              <p>
                <label htmlFor="InputPassword">New Password</label>
                <input type="password"
                  className="form-control"
                  id="InputPassword"
                  placeholder="New Password"
                  onChange={(e) => {this.handleChange(e, 'password')}} />
              </p>
              <p>
                <label htmlFor="ConfirmPassword">Confirm Password</label>
                <input type="password"
                  className="form-control"
                  id="ConfirmPassword"
                  placeholder="Confirm Password"
                  onChange={(e) => {this.handleChange(e, 'password2')}} />
              </p>
              <div>
                <Button onClick={ (e) => {this._submitChangePassword()}}>
                  Submit
                </Button>
              </div>
            </form>
          </div>
          <div className="col-md-8">&nbsp;</div>
          </div>
        </div>
      </div>

      </div>
    );
  }
}

export default connect(
  function(state) {
    return {
      authHeader: state.auth.header,
      postHeader: state.auth.postHeader,
      username: state.auth.user,
      isPasswordChangeError: state.account.isPasswordChangeError,
      message: state.account.message,
    };
  },
  function(dispatch) {
    return {
      changePasswordUser: function changePasswordUser(authHeader, p1 ,p2) {
        dispatch(changePassword(authHeader, p1, p2));
      }
    };
  }
)(User);

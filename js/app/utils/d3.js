import * as d3 from "d3";

function reduceOnArray(func, arr) {
  if (arr.length == 0) {
    return
  }
  if (arr.length == 1) {
    return func(arr[0]);
  }
  return arr.reduce(function(a, b) {
      return func(a, b);
  });
}


function calculateExtents(data) {
  var xMin = [],
      xMax = [],
      yMin = [],
      yMax = [];

  data.forEach(function(dataSet) {
      var xExtent = d3.extent(dataSet, function(d) { return d[0]; }),
          yExtent = d3.extent(dataSet, function(d) { return d[1]; });
      xMin.push(xExtent[0]);
      xMax.push(xExtent[1]);
      yMin.push(yExtent[0]);
      yMax.push(yExtent[1]);
  });

  return [
    reduceOnArray(Math.min, xMin), reduceOnArray(Math.max, xMax),
    reduceOnArray(Math.min, yMin), reduceOnArray(Math.max, yMax)]
}

export default {
  scatter: function scatter(params) {
    var margin = params.margin || {top: 30, right: 30, bottom: 30, left: 30, middle: 30},
        width =  (params.width || 960) - margin.left - margin.right,
        height = (params.height || 500) - margin.top - margin.bottom,
        xDomain = params.xDomain || null,
        yDomain = params.yDomain || null,
        duration = params.duration || 500,
        allLabels = params.allLabels || [],
        colorScheme = params.colorscheme || 'schemeCategory10';

    function canvasLinePlot(data, c) {
      var labels = Object.keys(data),
          data = labels.map(lbl => { return data[lbl]; }),
          useXDomain = null,
          useYDomain = null;

      if ((xDomain == null) || (yDomain == null)) {
          var extents = calculateExtents(data);
          if (xDomain == null) {
              useXDomain = [extents[0], extents[1]];
          }
          if (yDomain == null) {
              useYDomain = [extents[2], extents[3]];
          }
      }
      if (useXDomain == null) {
        useXDomain = xDomain;
      }
      if (useYDomain == null) {
        useYDomain = yDomain;
      }

      var x = d3.scaleLinear().range([0, width]).domain(useXDomain),
          y = d3.scaleLinear().range([height, 0]).domain(useYDomain),
          colors = d3.scaleOrdinal(d3[colorScheme]),
          xAxis = d3.axisBottom(x),
          yAxis = d3.axisLeft(y);

      if (allLabels.length) {
        colors.domain(allLabels);
      }

      c.attr("width", width + margin.left - margin.right)
          .attr("height", height + margin.top + margin.bottom);

      var t = d3.transition()
          .duration(duration);

      var plot = c.select(".plot-g");
      if (plot.empty()) {
        plot = c.append("g")
            .attr("class", "plot-g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
      }

      var xAxisG = plot.select(".x-axis-g");
      if (xAxisG.empty()) {
        var xAxisG = plot.append("g")
            .attr("class", "x-axis-g")
            .attr("transform", "translate(0," + height + ")");
      }
      xAxisG.call(xAxis);

      var yAxisG = plot.select(".y-axis-g");
      if (yAxisG.empty()) {
        var yAxisG = plot.append("g")
            .attr("class", "y-axis-g");
      }
      yAxisG.call(yAxis);

      var pointG = plot.selectAll(".point-g")
        .data(data);
      pointG.enter().append("g")
          .attr("class", "point-g")
        .merge(pointG)
          .each(drawPoints);

      function drawPoints(d, i) {
        var point = d3.select(this)
          .selectAll(".point")
          .data(d);
        point.enter().append("circle")
            .attr("class", "point")
            .attr("r", 3.5)
            .attr("cx", function(d) {
              return x(d[0]);
            })
            .attr("cy", function(d) {return y(d[1]);})
            .style("fill", function(d) {return colors(labels[i]);})
            .style("opacity", 0)
          .merge(point)
            .attr("cx", function(d) {return x(d[0]);})
            .attr("cy", function(d) {return y(d[1]);})
            .style("fill", function(d) {return colors(labels[i]);})
            .style("opacity", 1)
        pointG.exit()
          .attr("opacity", 0)
          .remove()
      }

    }

    return canvasLinePlot;
  }
};

require('isomorphic-fetch');
var Constants = require('../constants');

var funcs = {
    fetchProject: function fetchProject(argObj) {
        return fetch(
            Constants.api_urls.base+Constants.api_urls.get_projects.url+argObj.projectId+'/'+Constants.api_urls.suffix,
        {
            method: Constants.api_urls.get_projects.method,
            headers: argObj.authHeader,
            dataType: 'json'
        }).then(function(json) {
            return json.json();
        }).then(function(project) {
            argObj.project = project;
            return Promise.resolve(argObj);
        });
    },

    fetchProjects: function fetchProjects(argObj) {
        return fetch(
            Constants.api_urls.base+Constants.api_urls.get_projects.url+Constants.api_urls.suffix,
        {
            method: Constants.api_urls.get_projects.method,
            headers: argObj.authHeader,
            dataType: 'json'
        }).then(function(json) {
            return json.json();
        }).then(function(results) {
            argObj.projects = results.results;
            return Promise.resolve(argObj);
        });
    },

    fetchAnalysis: function fetchAnalysis(argObj) {
        return fetch(
            Constants.api_urls.base+Constants.api_urls.get_analyses.url+argObj.analysisId+'/'+Constants.api_urls.suffix,
        {
            method: Constants.api_urls.get_analyses.method,
            headers: argObj.authHeader,
            dataType: 'json'
        }).then(function(json) {
            return json.json();
        }).then(function(analysis) {
            argObj.analysis = analysis;
            return Promise.resolve(argObj);
        });
    },

    fetchAnalyses: function fetchAnalyses(argObj) {
        return fetch(
            Constants.api_urls.base+Constants.api_urls.get_analyses.url+Constants.api_urls.suffix,
        {
            method: Constants.api_urls.get_analyses.method,
            headers: argObj.authHeader,
            dataType: 'json'
        }).then(function(json) {
            return json.json();
        }).then(function(results) {
            argObj.analyses = results.results
            return Promise.resolve(argObj);
        });
    },

    fetchAnalysisSave: function fetchAnalysisSave(argObj) {
        return fetch(
            Constants.api_urls.base+Constants.api_urls.get_analysis_saves.url+argObj.analysisSaveId+'/'+Constants.api_urls.suffix,
        {
            method: Constants.api_urls.get_analysis_saves.method,
            headers: argObj.authHeader,
            dataType: 'json'
        }).then(function(json) {
            return json.json();
        }).then(function(analysisSave) {
            analysisSave.configuration = JSON.parse(analysisSave.configuration);
            argObj.analysisSave = analysisSave;
            return Promise.resolve(argObj);
        });
    },

    fetchAnalysisSaves: function fetchAnalysisSaves(argObj) {
        return fetch(
            Constants.api_urls.base+Constants.api_urls.get_analysis_saves.url+Constants.api_urls.suffix,
        {
            method: Constants.api_urls.get_analysis_saves.method,
            headers: argObj.authHeader,
            dataType: 'json'
        }).then(function(json) {
            return json.json();
        }).then(function(results) {
            argObj.analysisSaves = results.results.map(function(result) {
                result.configuration = JSON.parse(result.configuration);
                return result
            });
            return Promise.resolve(argObj);
        });
    },

    addAnalysisSave: function addAnalysisSave(argObj) {
        argObj.analysisSave.configuration = JSON.stringify(argObj.analysisSave.configuration);
        return fetch(
            Constants.api_urls.base+Constants.api_urls.add_analysis_save.url,
            {
                method: Constants.api_urls.add_analysis_save.method,
                headers: argObj.postHeader,
                body: JSON.stringify(argObj.analysisSave)
            });
    }
};

module.exports = funcs;

var React = require("react/addons"),
    SpecUtils = require('./spec-utils.js'),
    Menu = require('./../app/components/ActionMenu.js');

describe("ActionMenu", function() {
    var renderedMenu,
        menuProps = {
            menuItems: [
                {id: "1", name: "one", description: "one desc"},
                {id: "2", name: "two", description: "two desc"}
            ]
        };

    beforeEach(function() {
        renderedMenu = SpecUtils.createComponent(Menu, menuProps);
    });

    it("should render two items", function() {
        var renderedItems = renderedMenu.props.children;
        expect(renderedItems.length).toEqual(2);
    });

});

var React = require("react/addons"),
    SpecUtils = require('./spec-utils.js'),
    Title = require('./../app/components/PageTitle.js');

describe("Title", function() {
    var renderedTitle,
        titleProps = {
            title: 'title',
            subtitle: 'subtitle'
        };

    beforeEach(function() {
        renderedTitle = SpecUtils.createComponent(Title, titleProps);
    });

});

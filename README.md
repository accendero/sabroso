# sabroso #

# Current version: 0.5.0 (prototype, enhanced development environment) #

Sabroso is a software framework designed to make it easy to develop full-functionality, custom, web-based data visualization applications in rapid time. 

There are two core components of sabroso:

* A Python backend, implemented in Django-REST, which stores user data and performs custom, requested data transformations. The intended use of this backend is for the developer to insert fully custom data analysis pipelines for use within their application. It is designed with the use of the SciPy stack in mind, so that machine learning and complex statistical operations should be easily integrated into workflows.

* A React/Redux javascript front end, which communicates with the backend and allows for uses to push data into the system and interact with custom data visualizations defined by the developer.

System features:

* User management and authentication
* Data management through the use of projects
* Custom visualization and interactivity (now D3 and Vega enabled)
* Visualization and project sharing 

New in 0.5.0:

* Reports - add multiple analysis saves with annotations (HTML support)

New in 0.4.0:

* Enhanced plotting controls and configuration
* Shimming for reporting system, which will come in future release
* D3 plotting capabilities
* Reskin

We have started a wiki with information on installing, working with and implementing systems with sabroso, which can be found here:

https://bitbucket.org/accendero/sabroso/wiki/Home

import yaml
import json
from django.core.management.base import BaseCommand
from web.sabroso_rest.models import DataType, AnalysisType
from mongoengine import DoesNotExist

class Command(BaseCommand):
    help = 'Add data types and analysis types to database.'

    def add_arguments(self, parser):
        parser.add_argument('configuration_file', type=str)

    def handle(self, *args, **options):
        with open(options['configuration_file']) as yamlf:
            configuration = yaml.load(yamlf)

        data_types = configuration['data_types']
        data_type_key = {}
        for data_type in data_types:
            try:
                new_type = DataType.objects.get(name=data_type)
            except:
                new_type = DataType(name=data_type)
                new_type.save()
            data_type_key[new_type.name] = new_type

        for analysis_type in configuration['analysis_types']:
            new_types = []
            for data_type in analysis_type['data_types']:
                new_types.append(data_type_key[data_type])
            analysis_type['data_types'] = new_types
            analysis_type['plot_configuration'] = json.dumps(analysis_type['plot_configuration'])
            analysis_type['controls'] = json.dumps(analysis_type['controls'])
            analysis_type['transformation_configuration'] = json.dumps(analysis_type['transformation_configuration'])
            try:
                new_analysis_type = AnalysisType.objects.get(name=analysis_type['name'])
            except:
                data_types = analysis_type.pop('data_types')
                new_analysis_type = AnalysisType(**analysis_type)
                new_analysis_type.save()
                new_analysis_type.data_types.add(*data_types)
            else:
                for key in analysis_type:
                    setattr(new_analysis_type, key, analysis_type[key])
            new_analysis_type.save()
        self.stdout.write(self.style.SUCCESS('Successfully added data.'))

import csv

from django.core.management.base import BaseCommand
import pymongo

from web.sabroso_rest.models import Project, DataType

class Command(BaseCommand):
    help = 'Add csv into project data.'

    def add_arguments(self, parser):
        parser.add_argument('project_name', type=str)
        parser.add_argument('data_type_name', type=str)
        parser.add_argument('csv_file', type=str)

    def handle(self, *args, **options):
        mongo_connection = pymongo.MongoClient('mongodb://localhost')
        db = mongo_connection['sabroso_django']
        project_id = str(Project.objects.get(name=options['project_name']).id)
        data_type_id = str(DataType.objects.get(name=options['data_type_name']).id)
        project_data = "project_data_" + data_type_id

        with open(options['csv_file']) as csvf:
            rows = csv.DictReader(csvf)
            for row in rows:
                for key in row:
                    row[key] = float(row[key])
                row['project_id'] = project_id
                db[project_data].insert_one(row)

        self.stdout.write(self.style.SUCCESS('Successfully added data.'))

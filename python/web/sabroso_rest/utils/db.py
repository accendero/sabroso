from django.conf import settings
from django.db import connections

from .settings import *


def get_db():
    return PROJECT_DATA_BACKEND_GET_DB[settings.PROJECT_DATA_BACKEND]()


def get_columns(project, bucket):
    return PROJECT_GET_COLUMNS[settings.PROJECT_DATA_BACKEND](project, bucket)


def get_bucket_data(bucket, query, return_cursor):
    return PROJECT_BUCKET_DATA_ACCESSORS[settings.PROJECT_DATA_BACKEND](
        bucket, query, return_cursor
    )


def get_project_data_db():
    return connections[settings.DATABASES['project_data']['NAME']].cursor()


def get_project_data(project, links, data_type, return_cursor):
    return PROJECT_DATA_ACCESSORS[settings.PROJECT_DATA_BACKEND](
        project, links, data_type, return_cursor
    )


def get_distinct_project_ids(column, links, bucket_id):
    b_id = str(bucket_id)
    results = get_distinct_query(bucket_id=b_id, column='project_id', find_in=links)
    return results


def get_distinct_query(cursor, column, find_in):
    return PROJECT_DATA_QUERIES['distinct'][settings.PROJECT_DATA_BACKEND](cursor, column, find_in)


def get_data_collection(bucket_id):
    return PROJECT_DATA_COLLECTION[settings.PROJECT_DATA_BACKEND](bucket_id)


def insert_many(bucket_id, data):
    return PROJECT_DATA_INSERT_MANY[settings.PROJECT_DATA_BACKEND](bucket_id, data)


def delete_many(bucket_id, where_dict):
    return PROJECT_DATA_DELETE[settings.PROJECT_DATA_BACKEND](bucket_id, where_dict)

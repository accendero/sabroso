import mongoengine
from django.conf import settings


def get_db():
    return mongoengine.connection.get_db()


def get_data_collection(bucket_id):
    db = get_db()
    data_collection_name = settings.PROJECT_DATA_PREFIX + str(bucket_id)
    return db[data_collection_name]


def get_data_type_from_collection(collection):
    name = collection.name
    data_type = name.replace(settings.PROJECT_DATA_PREFIX, '', 1)
    return data_type


def get_distinct_query(bucket_id, column, find_in):
    cursor = get_data_collection(bucket_id)
    in_param = {column: {"$in": find_in}}
    return cursor.distinct(column, in_param)


def get_project_collection(bucket_id):
    b_id = str(bucket_id)
    try:
        col = get_db()[''.join([settings.PROJECT_DATA_PREFIX, b_id])]
    except:
        col = get_data_collection(b_id)
    return col


def get_columns(project, bucket):
    col = get_project_collection(bucket.id)
    first = col.find_one()
    try:
        return list(first.keys())
    except AttributeError:
        return list()


def get_data(bucket, query, return_cursor):
    col = get_project_collection(bucket.id)
    if return_cursor:
        return col.find(query[0], query[1])
    return [data for data in col.find(query[0], query[1])]


def insert_many(bucket, data):
    collection = get_data_collection(bucket.id)
    result = collection.insert_many(data)
    return [str(item) for item in result.inserted_ids]


def delete_many(bucket_id, where_dict):
    collection = get_data_collection(bucket_id)
    result = collection.delete_many(where_dict)
    return result.deleted_count

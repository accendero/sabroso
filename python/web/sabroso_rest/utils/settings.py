from web.sabroso_rest.utils import mongo
from web.settings import DATA_BACKEND_MONGO

PROJECT_DATA_BACKEND_GET_DB = {
    DATA_BACKEND_MONGO: mongo.get_db,
}

PROJECT_BUCKET_DATA_ACCESSORS = {
    DATA_BACKEND_MONGO: mongo.get_data,
}

PROJECT_DATA_QUERIES = {
    'distinct': {
        DATA_BACKEND_MONGO: mongo.get_distinct_query,
    },
}

PROJECT_DATA_COLLECTION = {
    DATA_BACKEND_MONGO: mongo.get_data_collection,
}

PROJECT_DATA_INSERT_MANY = {
    DATA_BACKEND_MONGO: mongo.insert_many,
}

PROJECT_DATA_DELETE = {
    DATA_BACKEND_MONGO: mongo.delete_many,
}

PROJECT_GET_COLUMNS = {
    DATA_BACKEND_MONGO: mongo.get_columns,
}

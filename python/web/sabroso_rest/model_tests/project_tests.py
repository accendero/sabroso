from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from djsabroso.api.serializers import ProjectSerializer
from djsabroso.api.models import Project


tu = 'test_user'
tp = 'test_password'
te = 'test_user@accendero.com'


class CreateProjectTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.data = {'name':'TestProjectCreate'}

    def test_can_create_project(self):
        response = self.client.post('v1/projects', self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

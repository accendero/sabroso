from django.contrib.auth.models import User
from rest_framework import serializers

from web.sabroso_rest.models import (
    DataType, AnalysisType, Analysis, AnalysisSave, Project,
    Report, ReportItem, DataBucket
)


def get_owner(obj):
    obj_type = obj.__class__.__name__

    if obj_type == 'Analysis':
        return obj.project.owner
    elif obj_type == 'AnalysisSave':
        return obj.analysis.project.owner
    elif obj_type == 'Project':
        return obj.owner
    elif obj_type == 'DataBucket':
        return obj.owner
    else:
        return None


class UserIsOwnerMixin(object):
    def get_current_user_is_owner(self, obj):
        owner = get_owner(obj)
        request = self.context.get("request", False)
        if request and hasattr(request, "user"):
            return owner == request.user
        return False


class UserSerializerReadOnly(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username',)
        read_only_fields = ('id', 'username',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password', 'is_superuser', 'is_staff')
        read_only_fields = ('id',)
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            is_superuser=validated_data['is_superuser'],
            is_staff=validated_data['is_staff'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class DataTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataType
        fields = '__all__'


class AnalysisTypeSerializer(serializers.ModelSerializer):
    data_types = DataTypeSerializer(many=True, read_only=True)
    active_projects = serializers.ListField(read_only=True)

    class Meta:
        model = AnalysisType
        fields = (
            'id', 'name', 'data_types', 'plot_configuration', 'controls', 'transformation_configuration', 'plugin',
            'active_projects', 'available_method', 'update_configuration')


class AnalysisSerializer(UserIsOwnerMixin, serializers.ModelSerializer):
    current_user_is_owner = serializers.SerializerMethodField(read_only=True)
    shared_by = UserSerializerReadOnly(read_only=True)

    class Meta:
        model = Analysis
        fields = (
            'id', 'name', 'description',
            'created_on', 'project', 'analysis_type',
            'buckets', 'current_user_is_owner', 'shared_by',
        )


class AnalysisSaveSerializer(UserIsOwnerMixin, serializers.ModelSerializer):
    current_user_is_owner = serializers.SerializerMethodField(read_only=True)
    shared_by = UserSerializerReadOnly(read_only=True)

    class Meta:
        model = AnalysisSave
        fields = '__all__'


class ProjectSerializer(UserIsOwnerMixin, serializers.ModelSerializer):
    analyses = AnalysisSerializer(many=True, read_only=True)
    current_user_is_owner = serializers.SerializerMethodField(read_only=True)
    shared_by = UserSerializerReadOnly(read_only=True)

    class Meta:
        model = Project
        fields = ('id', 'owner', 'name', 'description', 'created_on',
                  'analyses', 'current_user_is_owner', 'shared_by')


class AnalysisDeepSerializer(serializers.ModelSerializer):
    analysis_type = AnalysisTypeSerializer()
    project = ProjectSerializer()

    class Meta:
        model = Analysis
        fields = (
            'id', 'name', 'description',
            'created_on', 'project', 'analysis_type',
            'buckets',
        )


class AnalysisSaveDeepSerializer(serializers.ModelSerializer):
    analysis = AnalysisDeepSerializer(many=False, read_only=True)

    class Meta:
        model = AnalysisSave
        fields = '__all__'


class DataBucketSerializer(UserIsOwnerMixin, serializers.ModelSerializer):
    current_user_is_owner = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = DataBucket
        fields = '__all__'

    def create(self, validated_data):
        bucket = DataBucket(
            name=validated_data['name'],
            owner=validated_data['owner'],
            data_type=validated_data['data_type'],
            meta_data=validated_data['meta_data'] if 'meta_data' in validated_data else None
        )
        bucket.save()
        for pid in validated_data['projects']:
            bucket.projects.add(pid)
        bucket.save()
        return bucket


class ReportItemSerializer(serializers.ModelSerializer):
    super_serializers = {
        'AnalysisSave': AnalysisSaveDeepSerializer,
    }
    object_data = serializers.SerializerMethodField('item_to_json')

    class Meta:
        model = ReportItem
        fields = (
            'id', 'report', 'object_type', 'object_id', 'order', 'text',
            'text_location', 'object_data'
        )

    def item_to_json(self, obj):
        try:
            serializer = self.super_serializers[obj.object_type]
            return serializer(obj.get_instance_of()).data
        except:
            return {'data': obj.text}


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Report
        fields = (
            'id', 'project', 'name', 'description', 'created_on'
        )

class ReportDetailSerializer(serializers.ModelSerializer):

    report_items = ReportItemSerializer(
        source='get_report_items',
        many=True,
        read_only=True
    )

    project = ProjectSerializer()

    class Meta:
        model = Report
        fields = (
            'id', 'project', 'name', 'description', 'created_on',
            'report_items'
        )

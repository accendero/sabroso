from __future__ import unicode_literals

import datetime
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from web.sabroso_rest.utils.db import get_bucket_data, get_columns


class Project(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=255, blank=True, null=True)
    uuid=models.CharField(max_length=100, blank=True, default=uuid.uuid4)
    owner = models.ForeignKey(User, blank=True)
    shared_by = models.ForeignKey(User, blank=True, null=True, related_name='original_owner')
    created_on = models.DateTimeField(default=datetime.datetime.now)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def get_data(self, query, return_cursor=False):
        data = []
        for bucket in self.buckets.all():
            data.append(bucket.get_data(query, return_cursor=return_cursor))
        return data

    def get_columns(self, bucket):
        return get_columns(project=self, bucket=DataBucket.objects.get(id=bucket))

    def has_bucket(self, bucket_id):
        return self.buckets.filter(pk=bucket_id).count() > 0

    def get_connected_data(self):
        items = []
        for bucket in self.buckets.all():
            linkage = "LINK"
            if bucket.owner == self.owner:
                linkage = "SELF"
            items.append({
                'key': str(bucket.id),
                'value': "[{}] {}".format(linkage, bucket.name)
            })
        return items

    def share_with(self, user):
        try:
            project = Project.objects.get(uuid=self.uuid, owner=user)
        except ObjectDoesNotExist:
            project = Project(
                name=self.name,
                description=self.description,
                uuid=self.uuid,
                owner=user,
                shared_by=self.owner
            )
            try:
                project.save()
            except:
                return False

        for bucket in self.buckets.all():
            bucket.projects.add(project)

        return project

    class Meta:
        unique_together = (("uuid", "owner"),)


class DataType(models.Model):
    name = models.CharField(max_length=64)
    created_on = models.DateTimeField(default=datetime.datetime.now)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def get_projects(self):
        projects = set()
        buckets = DataBucket.objects.filter(data_type=self)
        for bucket in buckets:
            for project in bucket.projects.all():
                projects.add(project)
        return list(projects)

class AnalysisType(models.Model):
    name = models.CharField(max_length=128)
    data_types = models.ManyToManyField(DataType)
    plot_configuration = models.TextField()
    controls = models.TextField()
    transformation_configuration = models.TextField()
    available_method = models.TextField()
    update_configuration = models.CharField(max_length=255, blank=True)
    plugin = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def active_projects(self):
        projects = set()
        for data_type in self.data_types.all():
            buckets = DataBucket.objects.filter(data_type=data_type)
            for bucket in buckets:
                for project in bucket.projects.all():
                    projects.add(project.id)
        return list(projects)


class Analysis(models.Model):
    project = models.ForeignKey(Project)
    analysis_type = models.ForeignKey(AnalysisType)
    name = models.CharField(max_length=128)
    uuid=models.CharField(max_length=100, blank=True, default=uuid.uuid4)
    description = models.CharField(max_length=255, blank=True, null=True)
    shared_by = models.ForeignKey(User, blank=True, null=True, related_name='owned_analysis')
    created_on = models.DateTimeField(default=datetime.datetime.now)

    def share_with(self, user):
        share_project = self.project.share_with(user)

        if not share_project:
            return False

        try:
            analysis = Analysis.objects.get(uuid=self.uuid, project=share_project)
        except ObjectDoesNotExist:
            analysis = Analysis(
                name=self.name,
                description=self.description,
                uuid=self.uuid,
                project=share_project,
                analysis_type=self.analysis_type,
                shared_by=self.project.owner,
            )
            try:
                analysis.save()
            except:
                return False

        return analysis

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = (("uuid", "project"),)

class AnalysisSave(models.Model):
    analysis = models.ForeignKey(Analysis)
    configuration = models.TextField()
    created_on = models.DateTimeField(default=datetime.datetime.now)
    name = models.CharField(max_length=128)
    uuid=models.CharField(max_length=100, blank=True, default=uuid.uuid4)
    description = models.CharField(max_length=255, blank=True, null=True)
    shared_by = models.ForeignKey(User, blank=True, null=True, related_name='owned_save')

    def share_with(self, user):
        share_analysis = self.analysis.share_with(user)

        if not share_analysis:
            return False

        try:
            analysisSave = AnalysisSave.objects.get(uuid=self.uuid, analysis=share_analysis)
        except ObjectDoesNotExist:
            analysisSave = AnalysisSave(
                name=self.name,
                description=self.description,
                uuid=self.uuid,
                configuration=self.configuration,
                analysis=share_analysis,
                shared_by=self.analysis.project.owner,
            )
            try:
                analysisSave.save()
            except:
                return False

        return analysisSave

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = (("uuid", "analysis"),)

class DataBucket(models.Model):
    name = models.CharField(max_length=64, unique=True)
    owner = models.ForeignKey(User, blank=True)
    data_type = models.ForeignKey(DataType)
    meta_data = models.TextField(blank=True, null=True)
    projects = models.ManyToManyField(Project, related_name='buckets', blank=True)
    analysis = models.ManyToManyField(Analysis, related_name='buckets', blank=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def get_data(self, query, return_cursor):
        return get_bucket_data(bucket=self, query=query, return_cursor=return_cursor)


class Report(models.Model):
    project = models.ForeignKey(Project)
    created_on = models.DateTimeField(default=datetime.datetime.now)
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=255, blank=True, null=True)

    def share_with(self, user):
        share_project = self.project.share_with(user)
        if not share_project:
            return False

        share_report = Report(
            project=share_project,
            name=self.name,
            description=self.description
        )
        if not share_report.save():
            return False

        for item in self.reportitem_set.all():
            obj = item.get_instance_of().share_with(user)
            share_item = ReportItem(
                report=share_report,
                object_type=item.object_type,
                object_id=obj.id,
                order=item.order
            )
            share_item.save()

        return share_report

    def get_report_items(self):
        items = ReportItem.objects.filter(report=self).order_by('order')
        return items

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class ReportItem(models.Model):
    report = models.ForeignKey(Report)
    object_type = models.CharField(max_length=64)
    object_id = models.CharField(max_length=64)
    order = models.IntegerField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    # above, below, left, right
    text_location = models.CharField(max_length=5, blank=True, null=True)

    def get_class(self):
        classes = {
            'AnalysisSave': AnalysisSave,
            'None': None
        }
        try:
            return classes[self.object_type]
        except:
            return None

    def get_instance_of(self):
        klass = self.get_class()
        if klass is None:
            return self

        try:
            obj = self.get_class().objects.get(pk=self.object_id)
            return obj
        except:
            return None

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from djsabroso.api.models import AnalysisType
import logging
import json

tu = 'test_user'
tp = 'test_password'
te = 'test_user@accendero.com'

analysis_type_data = {
    'name': 'Test Analysis Type',
    'data_types': 'A, B, C',
    'configuration': "{'abc': 123, 'def': true}"
}

class AnalysisTypeTest(APITestCase):
    def setUp(self):
        self.logger = logging.getLogger("unittests")
        self.user = User.objects.create_user(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.analysis_type_data = analysis_type_data
        self.analysis_type = AnalysisType.objects.create(self.analysis_type_data)
        self.data = AnalysisTypeSerializer(self.analysis_type).data

    def test_can_get_analysis_type_list(self):
        response = self.client.get('/v1/analysis_types/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_analysis_type_detail(self):
        response = \
            self.client.get('/v1/analysis_types/' + str(self.analysis_type.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_create_analysis_type(self):
        response = self.client.post('/v1/analysis_types/', self.analysis_type_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_delete_analysis_type(self):
        response = self.client.delete('/v1/analysis_types/' + str(self.analysis_type.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

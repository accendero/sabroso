from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.authtoken.models import Token
from djsabroso.api.models import *

tu = 'test_user'
tp = 'test_password'
te = 'test_user@accendero.com'

class AuthTokenTestCase(APITestCase):
    def setUp(self):
        self.logger = logging.getLogger("unittests")
        self.user = User.objects.create_user(tu, te, tp)
        self.factory = APIRequestFactory()

    def test_receive_correct_auth_token(self):
        request = self.factory.post('/api-token-auth/', {'username': tu, 'password': tp})
        response = obtain_auth_token(request)
        token = response.data['token']
        user_token = Token.objects.get(user=self.user)
        self.assertEqual(token, user_token.key)

    def tearDown(self):
        pass

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from djsabroso.api.serializers import UserSerializer


su = 'super_user_test'
sp = 'super_user_password'
se = 'superusertest@accendero.com'


class CreateUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser(su, se, sp)
        self.client.login(username=su, password=sp)
        self.data = {
            'username': 'bbaggins',
            'first_name': 'bilbo',
            'last_name': 'baggins'
        }

    def test_can_create_user(self):
        response = self.client.post('v1/users', self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class ReadUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser(su, se, sp)
        self.client.login(username=su, password=sp)
        self.user = User.objects.create(username='bbaggins')

    def test_can_get_user_list(self):
        response = self.client.get('v1/users')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_user_detail(self):
        response = self.client.get('v1/users' + str(self.user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class UpdateUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser(su, se, sp)
        self.client.login(username=su, password=sp)
        self.user = User.objects.create(username='bbaggins', first_name='Bilbo')
        self.data = UserSerializer(self.user).data
        self.data.update({'first_name': 'Frodo'})

    def test_can_update_user(self):
        response = self.client.put('v1/users' + str(self.user.id), self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class DeleteUserTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser(su, se, sp)
        self.client.login(username=su, password=sp)
        self.user = User.objects.create(username="sgamgee")

    def test_can_delete_user(self):
        response = self.client.delete('v1/users/' + str(self.user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

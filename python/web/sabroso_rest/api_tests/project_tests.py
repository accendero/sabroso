from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from djsabroso.api.serializers import ProjectSerializer
from djsabroso.api.models import Project


tu = 'test_user'
tp = 'test_password'
te = 'test_user@accendero.com'


class CreateProjectTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.data = {'name':'TestProjectCreate'}

    def test_can_create_project(self):
        response = self.client.post('v1/projects', self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        Project.drop_collection()


class ReadProjectTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.project = Project.objects.create(name='TestProjectRead')

    def test_can_get_project_list(self):
        response = self.client.get('v1/projects')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_project_detail(self):
        response = self.client.get('v1/projects' + str(self.project.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        Project.drop_collection()


class UpdateProjectTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.project = Project.objects.create(name='TestProjectUpdate')
        self.data = ProjectSerializer(self.project).data
        self.data.update({'name': 'TestProjectUpdateRename'})

    def test_can_update_project(self):
        response = self.client.put('v1/projects' + str(self.project.id), self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        Project.drop_collection()


class DeleteProjectTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.project = Project.objects.create(name='TestProjectDelete')

    def test_can_delete_user(self):
        response = self.client.delete('v1/projects/' + str(self.project.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        Project.drop_collection()

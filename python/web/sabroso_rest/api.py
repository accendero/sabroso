"""
api.py

common API module for application, expected to contain
certain functions:
    transform(pivot_config, session)
"""

__author__ = "Alan Barber"

from application.functions import registry
from web.sabroso_rest.models import Project, DataBucket


def get_available(method, buckets, project):
    available = set([])

    try:
        project = Project.objects.get(id=project)
    except:
        raise TypeError('Project does not exist')

    for bucket in buckets:
        available = available | set(project.get_columns(bucket))

    ret = []
    for a in sorted(list(available)):
        na = registry.get(method)(a)
        if na:
            ret.append([na, a])
    return ret


def transform(pivot_config):
    # Shorthand
    filter_pms = pivot_config.get('filters', {})
    pivot = pivot_config.get('transform', [])
    pivot_meta_function = pivot_config.get('generateMeta', None)
    finalize_data = pivot_config.get('finalizeData', None)
    all_kwargs = pivot_config.get('kwargs', {})

    # Load bucket data
    all_data = {}
    for one_bucket in pivot_config['buckets']:
        bucket = DataBucket.objects.get(id=one_bucket)
        query = registry.get(pivot_config['filterFunction'])(bucket, pivot_config['columns'], **filter_pms)
        for qry, col in zip(query, pivot_config['columns']):
            all_data[(col, bucket.name, bucket.data_type.name)] = bucket.get_data(qry, return_cursor=True)

    # Convert to our data object using specified function
    fnToDataObj = pivot_config.get('toDataObject', None)
    if fnToDataObj:
        toDataObject = registry.get(fnToDataObj)
        finalObject = toDataObject(all_data)
    else:
        finalObject = all_data

    # Apply pivot functions
    for f in pivot:
        function_name = f['function']
        arguments = f['kwargs']
        arguments.update(all_kwargs)
        if len(arguments):
            finalObject = registry.get(function_name)(finalObject, **arguments)
        else:
            finalObject = registry.get(function_name)(finalObject)

    # Generate final form
    if finalize_data:
        finalObject = registry.get(finalize_data)(finalObject)

    return finalObject


def make_configuration(config, func, func_args):
    try:
        update_func = registry.get(func)
    except ValueError:
        update_func = None
    if update_func:
        return update_func(config, **func_args)
    else:
        raise TypeError('Update configuration function does not exist.')

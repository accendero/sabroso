import json
import re
import yaml

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404
from rest_framework import permissions, viewsets, status
from rest_framework.authentication import SessionAuthentication, \
    TokenAuthentication
from rest_framework.decorators import api_view, permission_classes, detail_route
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from rest_framework.response import Response

from web.sabroso_rest.models import (
    Project, DataType, AnalysisType, Analysis, AnalysisSave,
    Report, ReportItem,
    DataBucket
)
from web.sabroso_rest.serializers import (
    UserSerializer, DataTypeSerializer, AnalysisTypeSerializer,
    AnalysisSerializer,
    AnalysisSaveSerializer, ProjectSerializer, ReportSerializer,
    ReportDetailSerializer,
    ReportItemSerializer, DataBucketSerializer
)
from .api import transform, get_available, make_configuration
from .utils.db import insert_many, delete_many


def api_custom404(request):
    return JsonResponse({
        'status_code': 404,
        'error': 'Not found'
    })


def api_custom400(request):
    return JsonResponse({
        'status_code': 400,
        'error': 'Bad Request'
    })


def api_custom500(request):
    return JsonResponse({
        'status_code': 500,
        'error': 'Internal Server Error'
    })


class AllList(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        else:
            return request.user == obj


@permission_classes((DjangoModelPermissions,))
class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (DjangoModelPermissions,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(owner=user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def destroy(self, request, *args, **kwargs):
        try:
            project = self.get_object()
            # remove analyses & analysis saves
            analyses = Analysis.objects.filter(project=project)
            AnalysisSave.objects.filter(analysis__in=analyses).delete()
            analyses.delete()
            self.perform_destroy(project)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

    @detail_route(methods=['get'])
    def reports(self, request, pk=None):
        project = self.get_object()
        reports = Report.objects.filter(project=project)
        serializer = ReportSerializer
        data = serializer(reports, many=True, read_only=True).data
        return Response(data)

    @detail_route(methods=['get'])
    def saves(self, request, pk=None):
        project = self.get_object()
        analyses = Analysis.objects.filter(project=project)
        saves = AnalysisSave.objects.filter(analysis__in=analyses)
        serializer = AnalysisSaveSerializer
        data = serializer(saves, many=True, read_only=True).data
        return Response(data)


class DataTypeViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = DataType.objects.all()
    serializer_class = DataTypeSerializer


class DataBucketViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes(IsAuthenticated, )
    queryset = DataBucket.objects.all()
    serializer_class = DataBucketSerializer

    def get_queryset(self):
        user = self.request.user
        return DataBucket.objects.filter(
            Q(projects__in=Project.objects.filter(owner=user)) | Q(
                owner=user)).distinct()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class AnalysisViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Analysis.objects.all()
    serializer_class = AnalysisSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            analysis = self.get_object()
            # remove saves
            AnalysisSave.objects.filter(analysis=analysis).delete()
            self.perform_destroy(analysis)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class AnalysisTypeViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = AnalysisType.objects.all()
    serializer_class = AnalysisTypeSerializer


class AnalysisSaveViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = AnalysisSave.objects.all()
    serializer_class = AnalysisSaveSerializer


class ReportItemViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ReportItemSerializer
    queryset = ReportItem.objects.all()


class ReportViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Report.objects.all()

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return ReportDetailSerializer
        else:
            return ReportSerializer


# Project data lookup is now done by buckets instead of data_types
@api_view(['GET', 'PUT', 'POST', 'DELETE'])
@permission_classes((IsAuthenticated,))
def project_data_detail(request, pk, bucket_id=None):
    if request.method == 'GET':
        results = get_bucket_projects(bucket_id)
        return Response(results)

    if request.method == 'POST' or request.method == 'PUT':
        project = Project.objects.get(pk=pk)
        bucket = get_object_or_404(DataBucket, pk=bucket_id)
        data = request.data
        results = insert_many(bucket, data)
        return Response(results, status=status.HTTP_201_CREATED)

    if request.method == 'DELETE':
        deleted_count = delete_many(bucket_id, {'project_id': pk})
        return Response({'records-deleted': deleted_count},
                        status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def pivot_data(request):
    if not request.data:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    try:
        finished = transform(request.data)
    except TypeError as e:
        return Response({'error-message': str(e)},
                        status=status.HTTP_400_BAD_REQUEST)

    return Response(finished, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def update_configuration(request):
    if not request.data:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    analysis_type = get_object_or_404(AnalysisType,
                                      name=request.data['analysis_type'])
    plot_config = yaml.load(analysis_type.plot_configuration)
    if not 'function' in request.data:
        return Response(plot_config, status=status.HTTP_200_OK)
    try:
        finished = make_configuration(
            plot_config, request.data['function'],
            request.data['args'] if 'args' in request.data else {})
    except TypeError as e:
        return Response({'error-message': str(e)},
                        status=status.HTTP_400_BAD_REQUEST)

    return Response(finished, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def available_data(request):
    if not request.data:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    json_string = json.dumps(request.data)
    transform_data = json.loads(json_string)

    try:
        data = get_available(
            transform_data['method'],
            transform_data['buckets'],
            transform_data['project'])
        return Response(data, status=status.HTTP_200_OK)
    except TypeError as e:
        return Response({'error-message': str(e)},
                        status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def assign_buckets(request):
    if not request.data or not 'bucket' in request.data or not 'project' in request.data:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    try:
        project = Project.objects.get(id=int(request.data['project']))
    except:
        return Response(
            {'error': 'Could not find project'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        bucket = DataBucket.objects.get(id=int(request.data['bucket']))
    except:
        return Response(
            {'error': 'Could not find bucket'},
            status=status.HTTP_400_BAD_REQUEST
        )

    bucket.projects.add(project)
    return Response(
        {'success': 'Bucket modified.'},
        status=status.HTTP_200_OK
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def project_share(request):
    if not request.data:
        return Response(
            {'error': 'Please select a user and project to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        project_id = request.data['project']
        user_id = request.data['user']
    except KeyError:
        return Response(
            {'error': 'Please select a user and project to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    project = Project.objects.get(id=project_id) or None
    user = User.objects.get(id=user_id) or None

    if not project or not user:
        return Response(
            {'error': 'Please choose a valid project and user'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.user != project.owner:
        return Response(
            {'error': 'You may only share a project you own'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if project.share_with(user):
        return Response(
            {'success': 'Project successfully shared'},
            status=status.HTTP_200_OK
        )

    return Response(
        {'error': 'Could not share project'},
        status=status.HTTP_400_BAD_REQUEST
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def analysis_share(request):
    if not request.data:
        return Response(
            {'error': 'Please select a user and analysis to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        analysis_id = request.data['analysis']
        user_id = request.data['user']
    except KeyError:
        return Response(
            {'error': 'Please select a user and analysis to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    analysis = Analysis.objects.get(id=analysis_id) or None
    user = User.objects.get(id=user_id) or None

    if not analysis or not user:
        return Response(
            {'error': 'Please choose a valid analysis and user'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.user != analysis.project.owner:
        return Response(
            {'error': 'You may only share items from a project you own'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if analysis.share_with(user):
        return Response(
            {'success': 'Analysis successfully shared'},
            status=status.HTTP_200_OK
        )

    return Response(
        {'error': 'Could not share analysis'},
        status=status.HTTP_400_BAD_REQUEST
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def analysis_save_share(request):
    if not request.data:
        return Response(
            {'error': 'Please select a user and analysis save to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        analysis_save_id = request.data['analysis_save']
        user_id = request.data['user']
    except KeyError:
        return Response(
            {'error': 'Please select a user and analysis save to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    analysis_save = AnalysisSave.objects.get(id=analysis_save_id) or None
    user = User.objects.get(id=user_id) or None

    if not analysis_save or not user:
        return Response(
            {'error': 'Please choose a valid analysis save and user'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.user != analysis_save.analysis.project.owner:
        return Response(
            {'error': 'You may only share items from a project you own'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if analysis_save.share_with(user):
        return Response(
            {'success': 'Analysis save successfully shared'},
            status=status.HTTP_200_OK
        )

    return Response(
        {'error': 'Could not share analysis save'},
        status=status.HTTP_400_BAD_REQUEST
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def change_password(request):
    if not request.data:
        return Response(
            {'error': 'Please fill out the form'},
            status=status.HTTP_400_BAD_REQUEST
        )

    p1 = request.data['password1'] or None
    p2 = request.data['password2'] or None

    if not p1 or not p2:
        return Response(
            {'error': 'Please enter new password and confirm'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if p1 != p2:
        return Response(
            {'error': 'Passwords do not match'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if len(p1) < 1:
        return Response(
            {'error': 'Password must be at least 1 character'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if not re.search('[a-zA-Z0-9]', p1):
        return Response(
            {'error': 'Password must conatin at least a number or letter'},
            status=status.HTTP_400_BAD_REQUEST
        )

    user = request.user
    user.set_password(p1)
    user.save()

    return Response({'success': 'Password changed successfully'},
                    status=status.HTTP_200_OK)

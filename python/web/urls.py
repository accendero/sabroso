"""djsabroso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from web.sabroso_rest import views
from web.sabroso_rest.views import api_custom404, api_custom400, api_custom500

"""
Use our custom error code handlers
"""
handler404 = api_custom404
handler400 = api_custom400
handler500 = api_custom500

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet, base_name='user-view')
router.register(r'projects', views.ProjectViewSet)
router.register(r'analyses', views.AnalysisViewSet)
router.register(r'analysis_types', views.AnalysisTypeViewSet)
router.register(r'analysis_saves', views.AnalysisSaveViewSet)
router.register(r'data_types', views.DataTypeViewSet)
router.register(r'reports', views.ReportViewSet)
router.register(r'report_items', views.ReportItemViewSet)
router.register(r'buckets', views.DataBucketViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^v1/', include(router.urls)),
    url(r'^v1/project_data/(?P<bucket_id>[0-9a-z]+)/(?P<pk>[0-9a-z]+)/$', views.project_data_detail),
    url(r'^v1/project/share/$', views.project_share),
    url(r'^v1/projects/(?P<pk>[0-9a-z]+)/data/$', views.project_data_detail),
    url(r'^v1/projects/(?P<pk>[0-9a-z]+)/data/(?P<data_type>[a-zA-Z\ ]+)/$', views.project_data_detail),
    url(r'^v1/projects/(?P<pk>[0-9a-z]+)/data/(?P<bucket_id>[0-9]+)/$', views.project_data_detail),
    url(r'^v1/assign_buckets/$', views.assign_buckets),
    url(r'^v1/analysis/share/$', views.analysis_share),
    url(r'^v1/analysis_save/share/$', views.analysis_save_share),
    url(r'^v1/pivot/$', views.pivot_data),
    url(r'^v1/update_configuration/$', views.update_configuration),
    url(r'^v1/available/$', views.available_data),
    url(r'^v1/account/password/$', views.change_password),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', obtain_auth_token)
]

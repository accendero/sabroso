"""
functions.py

application functions to be imported into registry
"""

from .utils.registry import Registry

registry = Registry('TestFunctions')

#Registered functions
def mealDataConverter(data):
    ret = {}
    for oneSet in data:
        data_set = []
        for datum in data[oneSet]:
            del datum['_id']
            data_set.append(datum)
        ret[oneSet[0]] = data_set
    return ret

def mealBuildQuery(bucket, columns, **kwargs):
    queries = []
    for column in columns:
        query = ({}, None)
        queries.append(query)
    return queries

def mealAvailable(name):
    if name == 'meal':
        return name
    else:
        return None

def getListByGroups(data, *args, **kwargs):
    groups = kwargs['groups']
    keyname = kwargs['keyname']
    ret = {}
    for key in data:
        ret[key] = []
        for row in data[key]:
            if row[keyname] in groups:
                ret[key].append(row)
    return ret

def mealDataFrameToBins(data, **kwargs):
    ret = {}
    for key in data:
        ret[key] = []
        for row in data[key]:
            ret[key].append(row)
    return ret

registry.add('mealDataConverter', mealDataConverter)
registry.add('mealBuildQuery', mealBuildQuery)
registry.add('getListByGroups', getListByGroups)
registry.add('mealDataFrameToBins', mealDataFrameToBins)
registry.add('mealAvailable', mealAvailable)

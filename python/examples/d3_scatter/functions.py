"""
functions.py

application functions to be imported into registry
"""

from .utils.registry import Registry
import pandas
from .utils.pandas import cursorToDataFrame

registry = Registry('TestFunctions')

#Registered functions
def testDataConverter(data):
    ret = {}
    for oneSet in data:
        data_set = []
        for datum in data[oneSet]:
            del datum['_id']
            data_set.append(datum)
        ret[oneSet[0]] = pandas.DataFrame(data_set)
    return ret

def buildQuery(bucket, columns, **kwargs):
    queries = []
    for column in columns:
        query = ({}, {})
        query[1]['x'] = 1
        query[1][column.split('data_')[1]] = 1
        if 'xRangeControl' in kwargs:
            query[0]['x'] = {}
            if '0' in kwargs['xRangeControl'] and kwargs['xRangeControl']['0']:
                query[0]['x']['$gte'] = float(kwargs['xRangeControl']['0'])
            if '1' in kwargs['xRangeControl'] and kwargs['xRangeControl']['1']:
                query[0]['x']['$lte'] = float(kwargs['xRangeControl']['1'])
        queries.append(query)
    return queries

def passThrough(data, *args, **kwargs):
    for key in data:
        col = key.split("data_")[1]
        try:
            data[key][col] = data[key][col] + 1
        except KeyError:
            pass

    return data

def scatter_available(name):
    if name not in ['x', 'a', 'title', 'project_id', '_id', 'owner']:
        return 'data_' + name
    else:
        return None

def updateScatter(config, **kwargs):
    colorscheme = None
    if 'colorscheme' in kwargs:
        colorscheme = kwargs['colorscheme']['0'][0] if '0' in kwargs['colorscheme'] else None
    if colorscheme:
        config['colorscheme'] = colorscheme
    return config

def dataFrameToBins(data, **kwargs):
    ret = {}
    for col in ['data_y', 'data_z']:
        if col in data:
            ret[col] = []
    for data_set in data:
        data[data_set] = data[data_set].dropna(axis=0)
        for i, row in data[data_set].iterrows():
            if data_set == 'data_y':
                ret['data_y'].append([row['x'], row['y']])
            if data_set == 'data_z':
                ret['data_z'].append([row['x'], row['z']])
    return ret

registry.add('testDataConverter', testDataConverter)
registry.add('buildQuery', buildQuery)
registry.add('passThrough', passThrough)
registry.add('dataFrameToBins', dataFrameToBins)
registry.add('scatter_available', scatter_available)
registry.add('updateScatter', updateScatter)
